﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CommonLibrary.Interfaces
{
    /// <summary>
    /// Interface for data services
    /// </summary>
    /// <author>Dmitry Zhemkov</author>
    /// <date>22 April 2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public interface IDataService<T>
    {
        IEnumerable<T> GetData(Expression<Func<T, bool>> predicate);

        bool Add(T record);
        //What the first argument needed for? Will You edit primary key, seriously? M. Saharilenko.
        //I don't know!!!
        bool Edit(T recordOld, T recordNew);
        bool Remove(T record);
    }
}
