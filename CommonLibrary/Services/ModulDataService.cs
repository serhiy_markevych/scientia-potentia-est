﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for moduls.
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>15.04.2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class ModulDataService : BaseDataService, IDataService<ModulDataModel>
    {
        #region IService implementation

        public IEnumerable<ModulDataModel> GetData(System.Linq.Expressions.Expression<Func<ModulDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);
            using (var context = GetContext())
            {
                List<ModulDataModel> data = null;
                try
                {
                    data = context.Moduls
                        .Select(m => new ModulDataModel()
                        {
                            ID = m.ID,
                            Name = m.Name,
                            CanceledMark = m.CanceledMark,
                            Subject = new SubjectDataModel() { ID = m.Subject.ID, Name = m.Subject.Name, CanceledMark = m.Subject.CanceledMark }
                        }).Where(predicate).ToList<ModulDataModel>();
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException(string.Format(Errors.NotGetDataDB, e.Message, (e.InnerException != null) ? Errors.Exception + e.InnerException.Message : ""));
                }
                return data;
            }
        }

        public bool Add(ModulDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);
            using (var context = GetContext())
            {
                context.Moduls.AddObject(ConvertDMtoEntity(record));
                result = context.SaveChanges()>0;
            }
            return result;
        }

        public bool Edit(ModulDataModel recordOld, ModulDataModel recordNew)
        {
            bool result = false;
            if (recordNew == null)
                throw new ArgumentNullException("record", Errors.NotEditedObjectNull);
            using (var context = GetContext())
            {
                Modul modul = context.Moduls.Where(m => m.ID == recordNew.ID).FirstOrDefault();
                if (modul == null)
                    Add(recordNew);
                else
                {
                    modul.Name = recordNew.Name;
                    modul.CanceledMark = recordNew.CanceledMark;
                    if (recordNew.Subject != null)
                        modul.SubjectID = recordNew.Subject.ID;
                    try
                    {
                        result = context.SaveChanges()>0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, modul);
                        result = context.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }

        public bool Remove(ModulDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);
            using (var context = GetContext())
            {
                var modulToDel = context.Moduls.Where(m => m.ID == record.ID).FirstOrDefault();
                if (modulToDel != null)
                {
                    context.Moduls.DeleteObject(modulToDel);
                    result = context.SaveChanges() > 0;
                }
            }
            return result;
        }

        #endregion

        #region Entity<-->Data model conversions

        public static Modul ConvertDMtoEntity(ModulDataModel modulDM)
        {
            if (modulDM == null)
                throw new ArgumentNullException("modulDM", Errors.NotConvertedDMToEntityDMNull);
            Modul modul = new Modul();
            try
            {
                modul.ID = modulDM.ID;
                modul.Name = modulDM.Name;
                modul.CanceledMark = modulDM.CanceledMark;
                if (modulDM.Subject != null)
                    modul.SubjectID = modulDM.Subject.ID;
            }
            catch (Exception e)
            {
                throw new InvalidCastException(string.Format(Errors.NotConvertedDMToEntityPropertiesError, e.Message));
            }
            return modul;
        }

        public static ModulDataModel ConvertEntityToDM(Modul modulEntity)
        {
            if (modulEntity == null)
                throw new ArgumentNullException("modulEntity", Errors.NotConvertedEntityToDMEntiryNull);
            return new ModulDataModel(modulEntity.ID, modulEntity.Name, SubjectDataService.ConvertEntityToDM(modulEntity.Subject), modulEntity.CanceledMark);
        }

        #endregion
    }
}
