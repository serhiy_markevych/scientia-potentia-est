﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for subjects.
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>15.04.2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class SubjectDataService: BaseDataService, IDataService<SubjectDataModel>
    {
        #region IService implementation

        public IEnumerable<SubjectDataModel> GetData(System.Linq.Expressions.Expression<Func<SubjectDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);
            using (var context = GetContext())
            {
                List<SubjectDataModel> data = null;
                try
                {
                    data = context.Subjects.Select(s => new SubjectDataModel() { ID = s.ID, Name = s.Name, CanceledMark = s.CanceledMark }).Where(predicate).ToList<SubjectDataModel>();
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException(string.Format(Errors.NotGetDataDB, e.Message, (e.InnerException != null) ? Errors.Exception + e.InnerException.Message : ""));
                }
                return data;
            }
        }

        public bool Add(SubjectDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);
            using (var context = GetContext())
            {
                context.Subjects.AddObject(ConvertDMtoEntity(record, null));
                result = context.SaveChanges()>0;
            }
            return result;
        }

        public bool Edit(SubjectDataModel recordOld, SubjectDataModel recordNew)
        {
            bool result = false;
            if (recordNew == null)
                throw new ArgumentNullException("DataModel", Errors.NotEditedObjectNull);
            using (var context = GetContext())
            {
                Subject subject = context.Subjects.Where(s => s.ID == recordNew.ID).FirstOrDefault();
                if (subject == null)
                    Add(recordNew);
                else
                {
                    subject.Name = recordNew.Name;
                    subject.CanceledMark = recordNew.CanceledMark;
                    try
                    {
                        result = context.SaveChanges()>0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, subject);
                        result = context.SaveChanges()>0;
                    }
                }
            }
            return result;
        }

        public bool Remove(SubjectDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);
            using (var context = GetContext())
            {
                var subjectToDel = context.Subjects.Where(s => s.ID == record.ID).FirstOrDefault();
                var childs = context.Moduls.Where(m => m.SubjectID == record.ID).FirstOrDefault(); ;//comment this if cascade deleting was set in database
                if (subjectToDel != null && childs == null)
                {
                    context.Subjects.DeleteObject(subjectToDel);
                    result = context.SaveChanges()>0;
                }
            }
            return result;
        }

        #endregion

        #region Entity<-->Data model conversions

        public static Subject ConvertDMtoEntity(SubjectDataModel subjectDM, List<ModulDataModel> modulDMList = null)
        {
            if (subjectDM == null)
                throw new ArgumentNullException("subjectDM", Errors.NotConvertedDMToEntityDMNull);
            Subject subject = new Subject();
            try
            {
                subject.ID = subjectDM.ID;
                subject.Name = subjectDM.Name;
                subject.CanceledMark = subjectDM.CanceledMark;
                if (modulDMList != null)
                {
                    foreach (var modulDM in modulDMList)
                    {
                        subject.Moduls.Add(ModulDataService.ConvertDMtoEntity(modulDM));
                    }
                }
            }
            catch(Exception e)
            {
                throw new InvalidCastException(string.Format(Errors.NotConvertedDMToEntityPropertiesError, e.Message));
            }
            return subject;
        }

        public static SubjectDataModel ConvertEntityToDM(Subject subjectEntity)
        {
            if (subjectEntity == null)
                throw new ArgumentNullException("subjectEntity", Errors.NotConvertedEntityToDMEntiryNull);
            return new SubjectDataModel(subjectEntity.ID, subjectEntity.Name, subjectEntity.CanceledMark);
        }

        #endregion
    }
}
