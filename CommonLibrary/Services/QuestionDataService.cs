﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using System.Data;
using System.Data.Objects;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for questions
    /// </summary>
    /// <author>Chernysh Denis</author>
    /// <date>15 May 2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class QuestionDataService : BaseDataService, IDataService<QuestionDataModel>
    {
        #region Private Methods

        private void GetEntities(Question targetData, QuestionDataModel modelData)
        {
            if (targetData != null)
            {                
                targetData.OrdinalNumber = modelData.OrdinalNumber;
                targetData.Text = modelData.Text;
                targetData.Significance = modelData.Significance;
                targetData.Type = (int)modelData.Type;
                targetData.ModulID = modelData.ModulID;
                targetData.CanceledMark = modelData.CanceledMark;
                targetData.Explanation = modelData.Explanation;
            }
        }

        private Question GetEntities(QuestionDataModel modelData)
        {
            Question objectResult = new Question();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        #region IDataService implementation

        public IEnumerable<QuestionDataModel> GetData(System.Linq.Expressions.Expression<Func<QuestionDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

            using (var context = GetContext())
            {
                return
                    (from q in context.Questions
                     select new QuestionDataModel()
                        {
                            ID = q.ID,
                            OrdinalNumber = q.OrdinalNumber,
                            Text = q.Text,
                            Significance = q.Significance,
                            Type = (QuestionType)q.Type,
                            ModulID = q.ModulID,
                            CanceledMark = q.CanceledMark,
                            Explanation = q.Explanation
                        }).Where(predicate).ToList<QuestionDataModel>();
            }
        }

        public bool Add(QuestionDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

            using (var context = GetContext())
            {
                Question question = GetEntities(record);
                context.AddToQuestions(question);
                result = context.SaveChanges() > 0;
                record.ID = question.ID;
            }
            return result;
        }

        public bool Edit(QuestionDataModel recordOld, QuestionDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException("record", Errors.NotEditedObjectNull);

            using (var context = GetContext())
            {
                Question editedQuestion = context.Questions.Where(q => q.ID == recordNew.ID).FirstOrDefault();

                if (editedQuestion != null)
                {
                    GetEntities(editedQuestion, recordNew);
                    try
                    {
                        result = context.SaveChanges() > 0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, editedQuestion);
                        result = context.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }

        public bool Remove(QuestionDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

            using (var context = GetContext())
            {
                Question removedQuestion = context.Questions.Where(q => q.ID == record.ID).FirstOrDefault();

                if (removedQuestion != null)
                {
                    context.Questions.DeleteObject(removedQuestion);
                    result = context.SaveChanges() > 0;
                }
            }
            return result;
        }

        #endregion

    }
}
