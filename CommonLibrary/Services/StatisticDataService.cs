﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Objects;
using DataModelsLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Service for Statistic
    /// </summary>
    /// <author>Evgeny Drapoguz & Cherednychok Ivan</author>
    /// <date>11 May 2013</date>
   public class StatisticDataService : BaseDataService
    {
        public TestStatisticDataModel GetTestStatistic(int testID, int profileID, int userID)
        {
            using (var context = GetContext())
            {
                TestStatisticDataModel testStatistic = (from ts in context.Tests
                                                        where (ts.ID == testID) && (ts.StudentID == userID) && (ts.ProfileID == profileID)
                                                        select new TestStatisticDataModel()
                                                        {
                                                            Title = (from t in context.Profiles where (t.ID == profileID) select t.Name).FirstOrDefault(),
                                                            Author = (from a in context.Users where (a.ID == ts.ID) select a.LastName + " " + a.MiddleName + " " + a.FirstName).FirstOrDefault(),
                                                            DateOfPassing = (from d in context.Assign where d.ProfileID == profileID select d.TestDate).FirstOrDefault() ?? DateTime.Now,
                                                            UserFullName = (from u in context.Users where (u.ID == userID) select u.LastName + " " + u.MiddleName + " " + u.FirstName).FirstOrDefault(),
                                                            UserGroup = (from g in context.Groups where (g.ID == (from u in context.Users where (u.ID == userID) select u.GroupID).FirstOrDefault()) select g.Name).FirstOrDefault(),
                                                            TotalQuestions = ts.TotalQuestions,
                                                            TotalCorrectAnswers = ts.TotalAnswers,
                                                            QuestionsInfo = (from qi in context.TestQuestions
                                                                             where qi.TestID == testID
                                                                             select new QuestionInfo
                                                                             {
                                                                                 OrdinalNumber = qi.OrdinalNumber,
                                                                                 Title = (from q in context.Questions where (q.ID == qi.QuestionID) select q.Text).FirstOrDefault(),
                                                                                 IsCorrect = IsCorrectAnswer(qi.ID)
                                                                             }).ToList<QuestionInfo>()
                                                        }).FirstOrDefault();
                return testStatistic;
            }
        }

        public UserStatisticDataModel GetUserStatistic(int profileID, int userID)
        {
            using (var context = GetContext())
            {
                UserStatisticDataModel userStatistic = new UserStatisticDataModel
                {
                    Subject = (from t in context.Tests where t.ProfileID == profileID select t.Modul.Subject.Name).FirstOrDefault(),
                    TestInfo = (from a in context.Assign
                                where a.ProfileID == profileID
                                select new TestInfo
                                    {
                                        TestTitle = a.Profile.Name,
                                        AuthorName = (from u in context.Users where u.ID == a.TeacherID select u.LastName + " " + u.MiddleName + " " + u.FirstName).FirstOrDefault(),
                                        DateOfPassing = a.TestDate ?? DateTime.Now,
                                        StudentName = (from u in context.Users where u.ID == userID select u.LastName + " " + u.MiddleName + " " + u.FirstName).FirstOrDefault(),
                                        GroupName = a.Group.Name,
                                        IsPassed = true //TODO: Implement checks
                                    }).ToList<TestInfo>()
                };

                return userStatistic;
            }
        }

        public bool IsCorrectAnswer(int testQuestionID)
        {
            using (var context = GetContext())
            {
                bool result = false;

                QuestionDataModel itemQuestion = (from qt in context.Questions
                                                  where (qt.ID == testQuestionID)
                                                  select new QuestionDataModel()
                                                  {
                                                      ID = qt.ID,
                                                      OrdinalNumber = qt.OrdinalNumber,
                                                      Significance = qt.Significance,
                                                      Type = (QuestionType)qt.Type,
                                                      ModulID = qt.ModulID,
                                                      Explanation = qt.Explanation
                                                  }).FirstOrDefault<QuestionDataModel>();

                if (itemQuestion != null)
                {
                    switch (itemQuestion.Type)
                    {
                        case QuestionType.OneAnswer:

                            if ((from a in context.Answers
                                 where (a.ID == (from ta in context.TestAnswers
                                                 where ta.TestQuestionID == testQuestionID
                                                 select ta.AnswerID).FirstOrDefault())
                                 select a.Significance).FirstOrDefault() == 100)
                            {
                                result = true;
                            }
                            break;

                        case QuestionType.Classification:
                            break;

                        case QuestionType.Compliance:
                            break;

                        case QuestionType.MultipleAnswers:

                            List<int?> userAnswers = (from a in context.TestAnswers
                                                      where a.TestQuestionID == testQuestionID
                                                      select a.AnswerID).ToList<int?>();

                            int signAnswers = 0;

                            foreach (var itemAnswerID in userAnswers)
                            {
                                if (itemAnswerID != null)
                                {
                                    signAnswers += (from a in context.Answers
                                                    where a.ID == itemAnswerID
                                                    select a.Significance).FirstOrDefault();
                                }
                            }

                            if (signAnswers >= (100 - userAnswers.Count))
                            {
                                result = true;
                            }

                            break;

                        case QuestionType.Ordering:

                            List<TestAnswerDataModel> userTestAnswers = (from ta in context.TestAnswers
                                                                         where ta.TestQuestionID == testQuestionID
                                                                         select new TestAnswerDataModel()
                                                                         {
                                                                             AnswerID = ta.AnswerID,
                                                                             OrdinalNumber = ta.OrdinalNumber
                                                                         }).ToList<TestAnswerDataModel>();

                            result = true;

                            foreach (var itemUserAnswer in userTestAnswers)
                            {
                                int realNumber = (from a in context.Answers where a.ID == itemUserAnswer.AnswerID select a.OrdinalNumber).FirstOrDefault();

                                if (itemQuestion.OrdinalNumber != realNumber)
                                {
                                    result = false;
                                    break;
                                }
                            }

                            break;

                        case QuestionType.TypeAnswer:

                            TestAnswerDataModel userAnswer = (from ta in context.TestAnswers
                                                              where (ta.TestQuestionID == testQuestionID)
                                                              select new TestAnswerDataModel
                                                              {
                                                                  ID = ta.ID,
                                                                  TestQuestionID = ta.TestQuestionID,
                                                                  AnswerID = ta.AnswerID,
                                                                  Text = ta.Text
                                                              }).FirstOrDefault<TestAnswerDataModel>();

                            string realAnswer = (from a in context.Answers
                                                 where (a.ID == userAnswer.AnswerID)
                                                 select a.Text).FirstOrDefault();

                            if (String.Equals(userAnswer.Text, realAnswer, StringComparison.CurrentCultureIgnoreCase))
                            {
                                result = true;
                            }
                            break;
                    }
                }
                return result;
            }
        }
    }
}
