﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Service for User Role
    /// </summary>
    /// <author>Evgeny Drapoguz</author>
    /// <date>28 April 2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class RoleDataService : BaseDataService, IDataService<RoleDataModel>
    {
        #region Private Methods

        private void GetEntities(Role targetData, RoleDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.Name = modelData.Name;
                targetData.Role1 = Convert.ToInt32(modelData.Role);
                targetData.Description = modelData.Description;
                targetData.CanceledMark = modelData.CanceledMark;
            }
        }

        private Role GetEntities(RoleDataModel modelData)
        {
            Role objectResult = new Role();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        #region Implementation IBaseService
        public IEnumerable<RoleDataModel> GetData(Expression<Func<RoleDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

            using (var context = GetContext())
            {
                return
                    (from tmp in context.Roles
                     select new RoleDataModel()
                     {
                         ID = tmp.ID,
                         Name = tmp.Name,
                         Role = (DataModelsLibrary.UserRole)tmp.Role1,
                         Description = tmp.Description,
                         CanceledMark = tmp.CanceledMark,
                     }).Where(predicate).ToList<RoleDataModel>();
            }
        }

        public bool Add(RoleDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

            using (var context = GetContext())
            {
                Role role = GetEntities(record);

                context.AddToRoles(role);

                result = context.SaveChanges()>0;

                record.ID = role.ID;
            }
            return result;
        }

        public bool  Edit(RoleDataModel recordOld, RoleDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException("records", Errors.NotEditedObjectNull);

            using (var context = GetContext())
            {
                Role editedRole = context.Roles.Where(g => g.ID == recordOld.ID).FirstOrDefault();

                if (editedRole != null)
                {
                    GetEntities(editedRole, recordNew);
                    try
                    {
                        result = context.SaveChanges()>0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, editedRole);
                        result = context.SaveChanges()>0;
                    }
                }
            }
            return result;
        }

        public bool Remove(RoleDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

            using (var context = GetContext())
            {
                Role removedRole = context.Roles.Where(g => g.ID == record.ID).FirstOrDefault();

                if (removedRole != null)
                {
                    context.Roles.DeleteObject(removedRole);
                    result = context.SaveChanges()>0;
                }
            }
            return result;
        }
        #endregion

        #region Methods
        public RoleDataModel GetRole(UserRole role) {
            return this.GetData(r => r.Role == role).ToList<RoleDataModel>().FirstOrDefault<RoleDataModel>();            
        }
        #endregion
    }
}
