﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using System.Data;
using System.Data.Objects;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for answers
    /// </summary>
    /// <author>Chernysh Denis</author>
    /// <date>15 May 2013</date>
    ///  
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class AnswerDataService : BaseDataService, IDataService<AnswerDataModel>
    {
        #region Private Methods

        private void GetEntities(Answer targetData, AnswerDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.QuestionID = modelData.QuestionID;
                targetData.OrdinalNumber = modelData.OrdinalNumber;
                targetData.Text = modelData.Text;
                targetData.Significance = modelData.Significance;
                targetData.Group = modelData.Group;
                targetData.CanceledMark = modelData.CanceledMark;
            }
        }

        private Answer GetEntities(AnswerDataModel modelData)
        {
            Answer objectResult = new Answer();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        #region IDataService implementation
        public IEnumerable<AnswerDataModel> GetData(System.Linq.Expressions.Expression<Func<AnswerDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

            using (var context = GetContext())
            {
                return
                    (from a in context.Answers
                     select new AnswerDataModel()
                     {
                         ID = a.ID,
                         QuestionID = a.QuestionID,
                         OrdinalNumber = a.OrdinalNumber,
                         Text = a.Text,
                         Significance = a.Significance,
                         Group = a.Group,
                         CanceledMark = a.CanceledMark,
                         IsChecked = false
                     }).Where(predicate).ToList<AnswerDataModel>();
            }
        }

        public bool Add(AnswerDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

            using (var context = GetContext())
            {
                Answer answer = GetEntities(record);
                context.AddToAnswers(answer);
                result = context.SaveChanges() > 0;
                record.ID = answer.ID;
            }
            return result;
        }

        public bool Edit(AnswerDataModel recordOld, AnswerDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException("records", Errors.NotEditedObjectNull);

            using (var context = GetContext())
            {
                Answer editedAnswer = context.Answers.Where(a => a.ID == recordNew.ID).FirstOrDefault();

                if (editedAnswer != null)
                {
                    GetEntities(editedAnswer, recordNew);
                    try
                    {
                        result = context.SaveChanges() > 0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, editedAnswer);
                        result = context.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }

        public bool Remove(AnswerDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

            using (var context = GetContext())
            {
                Answer removedAnswer = context.Answers.Where(a => a.ID == record.ID).FirstOrDefault();

                if (removedAnswer != null)
                {
                    context.Answers.DeleteObject(removedAnswer);
                    result = context.SaveChanges()>0;
                }
            }
            return result;
        }

        #endregion
    }
}
