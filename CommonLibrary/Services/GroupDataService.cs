﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Service for Study Group
    /// </summary>
    /// <author>Evgeny Drapoguz</author>
    /// <date>28 April 2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class GroupDataService : BaseDataService, IDataService<GroupDataModel>
    {
        #region Private Methods

        private void GetEntities(Group targetData, GroupDataModel modelData)
        {
            if (targetData != null)
            {                
                targetData.Name = modelData.Name;
                targetData.CanceledMark = modelData.CanceledMark;
            }
        }

        private Group GetEntities(GroupDataModel modelData)
        {
            Group objectResult = new Group();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        #region Implementation IBaseService
        public IEnumerable<GroupDataModel> GetData(System.Linq.Expressions.Expression<Func<GroupDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

            using (var context = GetContext())
            {
                return
                    (from g in context.Groups
                     select new GroupDataModel()
                     {
                         ID = g.ID,
                         Name = g.Name,
                         CanceledMark = g.CanceledMark,
                     }).Where(predicate).ToList<GroupDataModel>();
            }
        }

        public bool Add(GroupDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

            using(var context = GetContext())
            {
                Group group = GetEntities(record);
                context.AddToGroups(group);
                result = context.SaveChanges()>0;
                record.ID = group.ID;
            }
            return result;
        }

        public bool Edit(GroupDataModel recordOld, GroupDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException("record", Errors.NotEditedObjectNull);

            using (var context = GetContext())
            {
                Group editedGroup = context.Groups.Where(g => g.ID == recordNew.ID).FirstOrDefault();

                if (editedGroup != null)
                {
                    GetEntities(editedGroup, recordNew);
                    try
                    {
                        result = context.SaveChanges()>0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, editedGroup);
                        result = context.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }

        public bool Remove(GroupDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

            using (var context = GetContext())
            {
                Group removedGroup = context.Groups.Where(g => g.ID == record.ID).FirstOrDefault();

                if (removedGroup != null)
                {
                    context.Groups.DeleteObject(removedGroup);
                    result = context.SaveChanges() > 0;
                }
            }
            return result;
        }
        #endregion

        #region Methods
        public GroupDataModel CreateGroup(string name, int canceledMark = 0)
        {
            GroupDataModel group = new GroupDataModel() { Name = name, CanceledMark = canceledMark };
            this.Add(group);

            return group;
        }

        public GroupDataModel GetGroup(string groupName)
        {
            return this.GetData(g => g.Name == groupName).FirstOrDefault<GroupDataModel>();
        }
        #endregion
    }
}
