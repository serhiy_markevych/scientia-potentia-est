﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using CommonLibrary.Interfaces;
using DataModelsLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// "Tests" table services
    /// </summary>
    ///     
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public sealed class TestDataService : BaseDataService, IDataService<TestDataModel>
    {
        #region Private Methods

        private void GetEntities(Test targetData, TestDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.ModulID = modelData.ModulID;
                targetData.StudentID = modelData.StudentID;
                targetData.TeacherID = modelData.TeacherID;
                targetData.ProfileID = modelData.ProfileID;
                targetData.Mode = (int)modelData.Mode;
                targetData.Date = modelData.Date;
                targetData.Duration = modelData.DurationTicks;
                targetData.Rating = modelData.Rating;
                targetData.TotalRating = modelData.TotalRating;
                targetData.TotalQuestions = modelData.TotalQuestions;
                targetData.TotalAnswers = modelData.TotalAnswers;
            }
        }

        private Test GetEntities(TestDataModel modelData)
        {
            Test objectResult = new Test();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        /// <summary>
        /// Get record of the table by condition based on a predicate
        /// </summary>
        /// <param name="predicate">Function to test each element for a condition</param>
        /// <returns>Records that matches the search condition</returns>
        public IEnumerable<TestDataModel> GetData(Expression<Func<TestDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {                
                return
                    (from t in context.Tests
                     select new TestDataModel()
                     {
                         ID = t.ID,
                         ModulID = t.ModulID,
                         StudentID = t.StudentID,
                         TeacherID = t.TeacherID,
                         ProfileID = t.ProfileID,
                         Mode = (TestMode)t.Mode,
                         Date = t.Date ?? DateTime.MinValue,
                         DurationTicks = t.Duration,
                         Rating = t.Rating,
                         TotalRating = t.TotalRating,
                         TotalAnswers = t.TotalAnswers,
                         TotalQuestions = t.TotalQuestions,
                     }).Where(predicate).ToArray();
            }
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public bool Add(TestDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var entity = GetEntities(record);
                context.Tests.AddObject(entity);
                result = context.SaveChanges() > 0;

                record.ID = entity.ID;
            }
            return result;
        }

        /// <summary>
        /// Edits record in a table
        /// </summary>
        /// <param name="recordOld">Record to edit</param>
        /// <param name="recordNew">Record with new values</param>
        public bool Edit(TestDataModel recordOld, TestDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var entity = context.Tests.Where(t => t.ID == recordOld.ID).SingleOrDefault();
                if (entity != null)                {
                    GetEntities(entity, recordNew);
                    try
                    {
                        result = context.SaveChanges() > 0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, entity);
                        result = context.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Removes record from a table
        /// </summary>
        /// <param name="record">Record that must be removed</param>
        public bool Remove(TestDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var testObj = context.Tests.Where(t => t.ID == record.ID).SingleOrDefault();
                if (testObj != null)
                {
                    context.Tests.DeleteObject(testObj);
                    result = context.SaveChanges() > 0;
                }
            }
            return result;
        }
    }
}
