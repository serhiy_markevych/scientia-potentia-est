﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using CommonLibrary.Interfaces;
using DataModelsLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// "TestAnswers" table services
    /// </summary>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public sealed class TestAnswerDataService : BaseDataService, IDataService<TestAnswerDataModel>
    {
        #region Private Methods

        private void GetEntities(TestAnswer targetData, TestAnswerDataModel modelData)
        {
            if (targetData != null)
            {                
                targetData.OrdinalNumber = modelData.OrdinalNumber;
                targetData.TestQuestionID = modelData.TestQuestionID;
                targetData.Text = modelData.Text;
                targetData.AnswerID = modelData.AnswerID;
            }
        }

        private TestAnswer GetEntities(TestAnswerDataModel modelData)
        {
            TestAnswer objectResult = new TestAnswer();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        /// <summary>
        /// Get record of the table by condition based on a predicate
        /// </summary>
        /// <param name="predicate">Function to test each element for a condition</param>
        /// <returns>Records that matches the search condition</returns>
        public IEnumerable<TestAnswerDataModel> GetData(Expression<Func<TestAnswerDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                return (from t in context.TestAnswers
                        select new TestAnswerDataModel()
                        {
                            ID = t.ID,
                            AnswerID = t.AnswerID,
                            OrdinalNumber = t.OrdinalNumber,
                            TestQuestionID = t.TestQuestionID,
                            Text = t.Text
                        }).Where(predicate).ToArray();
            }
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public bool Add(TestAnswerDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var testAnswerObj = GetEntities(record);
                context.TestAnswers.AddObject(testAnswerObj);
                result = context.SaveChanges()>0;
                record.ID = testAnswerObj.ID;
            }
            return result;
        }

        /// <summary>
        /// Edits record in a table
        /// </summary>
        /// <param name="recordOld">Record to edit</param>
        /// <param name="recordNew">Record with new values</param>
        public bool Edit(TestAnswerDataModel recordOld, TestAnswerDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var testAnswerObj = context.TestAnswers.Where(t => t.ID == recordOld.ID).SingleOrDefault();

                if (testAnswerObj != null)
                {
                    GetEntities(testAnswerObj, recordNew);
                    try
                    {
                        result = context.SaveChanges()>0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, testAnswerObj);
                        result = context.SaveChanges()>0;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Removes record from a table
        /// </summary>
        /// <param name="record">Record that must be removed</param>
        public bool Remove(TestAnswerDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var testAnswersObj = context.TestAnswers.Where(t => t.ID == record.ID).SingleOrDefault();
                if (testAnswersObj != null)
                {
                    context.TestAnswers.DeleteObject(testAnswersObj);
                    result = context.SaveChanges()>0;
                }
            }
            return result;
        }
    }
}
