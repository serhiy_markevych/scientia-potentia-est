﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLibrary.Interfaces;
using DataModelsLibrary;
using System.Data;
using System.Data.Objects;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for Profiles
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>13.05.2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class ProfileDataService : BaseDataService, IDataService<ProfilesDataModel>
    {
        #region Private Methods

        private void GetEntities(Profile targetData, ProfilesDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.Name = modelData.Name;
                targetData.CanceledMark = modelData.CanceledMark;
                targetData.TimeLimit = modelData.TimeLimit;
                targetData.ShuffleQuestions = modelData.ShuffleQuestions;
                targetData.ShuffleAnswers = modelData.ShuffleAnswers;
                targetData.QuestionsCount = modelData.QuestionsCount;
                if (modelData.ID > 0)
                {
                    if (!targetData.MarkType.Equals((int)modelData.MarkType))
                    {
                        RatingScaleDataService rservice = new RatingScaleDataService();
                        rservice.RemoveForParent(modelData);
                    }
                    else targetData.MarkType = (int)modelData.MarkType;
                }
                else targetData.MarkType = (int)modelData.MarkType;
                
            }
        }

        private Profile GetEntities(ProfilesDataModel modelData)
        {
            Profile objectResult = new Profile();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion

        #region Implementation IBaseService
            public IEnumerable<ProfilesDataModel> GetData(System.Linq.Expressions.Expression<Func<ProfilesDataModel, bool>> predicate)
            {
                if (predicate == null)
                    throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

                using (var context = GetContext())
                {
                    return
                        (from p in context.Profiles
                         select new ProfilesDataModel()
                         {
                             ID = p.ID,
                             Name = p.Name,
                             CanceledMark = p.CanceledMark,
                             TimeLimit = p.TimeLimit,
                             ShuffleQuestions = p.ShuffleQuestions,
                             ShuffleAnswers = p.ShuffleAnswers,
                             QuestionsCount = p.QuestionsCount,
                             MarkType = (ProfileMarkType)p.MarkType,
                         }).Where(predicate).ToList<ProfilesDataModel>();
                }
            }

            public bool Add(ProfilesDataModel record)
            {
                bool result = false;
                if (record == null)
                    throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

                using (var context = GetContext())
                {
                    Profile profile = GetEntities(record);
                    context.AddToProfiles(profile);
                    result = context.SaveChanges() > 0;
                }
                return result;
            }

            public bool Edit(ProfilesDataModel recordOld, ProfilesDataModel recordNew)
            {
                bool result = false;
                if (recordNew == null)
                    throw new ArgumentNullException("record", Errors.NotEditedObjectNull);

                using (var context = GetContext())
                {
                    Profile profile = (from prf in context.Profiles
                                       where prf.ID == recordNew.ID
                                       select prf).FirstOrDefault();
                    if (profile != null)
                    {
                        GetEntities(profile, recordNew);
                        try
                        {
                            result = context.SaveChanges() > 0;
                        }
                        catch (OptimisticConcurrencyException)
                        {
                            context.Refresh(RefreshMode.ClientWins, profile);
                            result = context.SaveChanges() > 0;
                        }
                    }
                }
                return result;
            }

            public bool Remove(ProfilesDataModel record)
            {
                bool result = false;
                if (record == null)
                    throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

                using (var context = GetContext())
                {
                    Profile profile = (from prf in context.Profiles
                                       where prf.ID == record.ID
                                       select prf).FirstOrDefault();

                    if (profile != null)
                    {
                        RemoveChild(context, profile);
                        context.Profiles.DeleteObject(profile);
                        result = context.SaveChanges() > 0;
                    }
                }
                return result;
            }

            private void RemoveChild(ScientiaPotentiaEstEntities context, Profile profile)
            {
                List<RatingScale> profileRS = (from rsdm in context.RatingScales
                                               where rsdm.ProfileID == profile.ID
                                               select rsdm).ToList();

                if (profileRS != null)
                {
                    foreach (var rs in profileRS)
                        context.RatingScales.DeleteObject(rs);
                }
            }
        #endregion
    }
}
