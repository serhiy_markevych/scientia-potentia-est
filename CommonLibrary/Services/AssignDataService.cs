﻿using CommonLibrary.Interfaces;
using DataModelsLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Text;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for Assign
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>14.05.2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class AssignDataService : BaseDataService, IDataService<AssignDataModel>
    {

        #region Private Methods

        private void GetEntities(Assign targetData, AssignDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.GroupID = (modelData.Group != null) ? modelData.Group.ID : -1;
                targetData.TeacherID = (modelData.Teacher != null) ? modelData.Teacher.ID : -1;
                targetData.ProfileID = (modelData.Profile != null) ? modelData.Profile.ID : -1;
                targetData.ModulID = (modelData.Module != null) ? modelData.Module.ID : -1;
                targetData.TestDate = modelData.TestDate;
            }
        }

        private Assign GetEntities(AssignDataModel modelData)
        {
            Assign objectResult = new Assign();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion


        public IEnumerable<AssignDataModel> GetData(System.Linq.Expressions.Expression<Func<AssignDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

            using (var context = GetContext())
            {
                return
                    (from assign in context.Assign
                     select new AssignDataModel()
                     {
                         ID = assign.ID,
                         Group = new GroupDataModel() { ID = assign.Group.ID, Name = assign.Group.Name, CanceledMark = assign.Group.CanceledMark },
                         Teacher = new UserDataModel() { ID = assign.User.ID, Username = assign.User.UserName, Password = assign.User.Password, Email = assign.User.Email, CanceledMark = assign.User.CanceledMark, FirstName = assign.User.FirstName, LastName = assign.User.LastName, MiddleName = assign.User.MiddleName, Group = new GroupDataModel() { ID = assign.User.Group.ID, Name = assign.User.Group.Name, CanceledMark = assign.User.Group.CanceledMark } },
                         Profile = new ProfilesDataModel() { ID = assign.Profile.ID, Name = assign.Profile.Name, CanceledMark = assign.Profile.CanceledMark, TimeLimit = assign.Profile.TimeLimit, ShuffleQuestions = assign.Profile.ShuffleQuestions, ShuffleAnswers = assign.Profile.ShuffleAnswers, QuestionsCount = assign.Profile.QuestionsCount, MarkType = (ProfileMarkType)assign.Profile.MarkType },
                         Module = new ModulDataModel() { ID = assign.Modul.ID, Name = assign.Modul.Name, CanceledMark = assign.Modul.CanceledMark, Subject = new SubjectDataModel() { ID = assign.Modul.Subject.ID, Name = assign.Modul.Subject.Name, CanceledMark = assign.Modul.Subject.CanceledMark } },
                         TestDate = assign.TestDate,
                     }).Where(predicate).ToList<AssignDataModel>();
            }
        }

        public bool Add(AssignDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

            using (var context = GetContext())
            {
                Assign assign = GetEntities(record);

                context.AddToAssign(assign);
                result = context.SaveChanges() > 0;

                Queue<MailDataModel> qmail = GetMailData(record);
                if (qmail != null) Mailer.SendMail(qmail);
            }
            return result;
        }

        public bool Edit(AssignDataModel recordOld, AssignDataModel recordNew)
        {
            bool result = false;
            if (recordNew == null)
                throw new ArgumentNullException("record", Errors.NotEditedObjectNull);

            using (var context = GetContext())
            {
                Assign assign = (from asn in context.Assign
                                 where asn.ID == recordNew.ID
                                 select asn).FirstOrDefault();
                if (assign != null)
                {
                    GetEntities(assign, recordNew);
                    try
                    {
                        result = context.SaveChanges() > 0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, assign);
                        result = context.SaveChanges() > 0;
                    }
                }
            }
            return result;
        }

        public bool Remove(AssignDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

            using (var context = GetContext())
            {
                Assign assign = (from asn in context.Assign
                                 where asn.ID == record.ID
                                 select asn).FirstOrDefault();

                if (assign != null)
                {
                    context.Assign.DeleteObject(assign);
                    result=context.SaveChanges()>0;
                }
            }
            return result;
        }

        public Queue<MailDataModel> GetMailData(AssignDataModel record)
        {
            List<MailDataModel> lmail;
            Queue<MailDataModel> qmail = new Queue<MailDataModel>();
            using (var context = GetContext())
            {
                string _body = GetMailBody(record);
                lmail = (from dat in context.Users
                         where dat.GroupID == record.Group.ID
                         select new MailDataModel() { Body = _body, EmailAddress = dat.Email, Subject = "Testing data" }).ToList();
            }

            if (lmail != null)
            {
                foreach (MailDataModel mdm in lmail)
                {
                    qmail.Enqueue(mdm);
                }
            }

            return qmail;
        }

        public string GetMailBody(AssignDataModel record)
        {
            return String.Format(Messages.NewTestDateYourTeacher, record.Module.Name, record.TestDate, record.Teacher.FullName);
        }
    }
}
