﻿using CommonLibrary.Interfaces;
using DataModelsLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Text;
using ResourcesLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Data Service for RatingScale
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>14.05.2013</date>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public class RatingScaleDataService : BaseDataService, IDataService<RatingScaleDataModel>
    {
        #region Private Methods

        private void GetEntities(RatingScale targetData, RatingScaleDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.ProfileID = (modelData.Profile != null) ? modelData.Profile.ID : -1;
                targetData.Text = modelData.Text;
                targetData.LowBound = (modelData.Profile.MarkType.Equals(ProfileMarkType.Percent) && modelData.LowBound > 0) ? (modelData.LowBound / 100) : modelData.LowBound;
            }
        }

        private RatingScale GetEntities(RatingScaleDataModel modelData)
        {
            RatingScale objectResult = new RatingScale();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion
        #region Implementation IBaseService
            public IEnumerable<RatingScaleDataModel> GetData(System.Linq.Expressions.Expression<Func<RatingScaleDataModel, bool>> predicate)
            {
                if (predicate == null)
                    throw new ArgumentNullException("predicate", Errors.NotSelectedPredicateNull);

                using (var context = GetContext())
                {
                    return
                        (from rs in context.RatingScales
                         select new RatingScaleDataModel()
                         {
                             ID = rs.ID,
                             Profile = new ProfilesDataModel() { ID = rs.Profile.ID, Name = rs.Profile.Name, CanceledMark = rs.Profile.CanceledMark, TimeLimit = rs.Profile.TimeLimit, ShuffleQuestions = rs.Profile.ShuffleQuestions, ShuffleAnswers = rs.Profile.ShuffleAnswers, QuestionsCount = rs.Profile.QuestionsCount, MarkType = (ProfileMarkType)rs.Profile.MarkType},
                             Text = rs.Text,
                             LowBound = rs.LowBound,
                         }).Where(predicate).ToList<RatingScaleDataModel>();
                }
            }

            public bool Add(RatingScaleDataModel record)
            {
                bool result = false;
                if (record == null)
                    throw new ArgumentNullException("record", Errors.NotAddedDataModelNull);

                using (var context = GetContext())
                {
                    RatingScale ratingscale = GetEntities(record);
                    context.AddToRatingScales(ratingscale);
                    result = context.SaveChanges()>0;
                }
                return result;
            }

            public bool Edit(RatingScaleDataModel recordOld, RatingScaleDataModel recordNew)
            {
                bool result = false;
                if (recordNew == null)
                    throw new ArgumentNullException("record", Errors.NotEditedObjectNull);

                using (var context = GetContext())
                {
                    RatingScale ratingscale = (from rts in context.RatingScales
                                               where rts.ID == recordNew.ID
                                               select rts).FirstOrDefault();
                    if (ratingscale != null)
                    {
                        GetEntities(ratingscale, recordNew);

                        try
                        {
                            result = context.SaveChanges()>0;
                        }
                        catch (OptimisticConcurrencyException)
                        {
                            context.Refresh(RefreshMode.ClientWins, ratingscale);
                            result = context.SaveChanges()>0;
                        }
                    }
                }
                return result;
            }

            public bool Remove(RatingScaleDataModel record)
            {
                bool result = false;
                if (record == null)
                    throw new ArgumentNullException("record", Errors.NotRemovedDataModelNull);

                using (var context = GetContext())
                {
                    RatingScale ratingscale = (from rts in context.RatingScales
                                               where rts.ID == record.ID
                                               select rts).FirstOrDefault();

                    if (ratingscale != null)
                    {
                        context.RatingScales.DeleteObject(ratingscale);
                        result = context.SaveChanges()>0;
                    }
                }
                return result;
            }

            public void RemoveForParent(ProfilesDataModel profile)
            {
                if (profile != null)
                {
                    List<RatingScaleDataModel> lrs = GetData(p => true).Where(p => p.Profile.ID == profile.ID).ToList();
                    foreach (var item in lrs)
                    {
                        Remove(item);
                    }
                }
            }
        #endregion
    }
}
