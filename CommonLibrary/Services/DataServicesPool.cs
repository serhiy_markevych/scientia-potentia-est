﻿using System;
using System.Collections.Generic;
using CommonLibrary.Interfaces;
using DataModelsLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// Pool of the data services. 
    /// Created by M. Saharilenko, 19.04.2013
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public static class DataServicesPool
    {
        private static volatile Dictionary<Type, object> _cachedTables = new Dictionary<Type, object>();

        #region Properties

        static public IDataService<SubjectDataModel> SubjectDataService
        {
            get
            {
                return Accessor<SubjectDataService>();
            }
        }

        static public IDataService<ModulDataModel> ModulDataService
        {
            get
            {
                return Accessor<ModulDataService>();
            }
        }

        static public IDataService<ProfilesDataModel> ProfileDataService
        {
            get
            {
                return Accessor<ProfileDataService>();
            }
        }

        static public IDataService<AssignDataModel> AssignDataService
        {
            get
            {
                return Accessor<AssignDataService>();
            }
        }

        static public IDataService<RatingScaleDataModel> RatingScaleDataService
        {
            get
            {
                return Accessor<RatingScaleDataService>();
            }
        }

        static public IDataService<GroupDataModel> GroupDataService
        {
            get
            {
                return Accessor<GroupDataService>();
            }
        }

        static public IDataService<UserDataModel> UserDataService
        {
            get
            {
                return Accessor<UserDataService>();
            }
        }

        //Add yours properties here

        #endregion

        #region Methods

        static private void GetData<T>()
            where T : new()
        {
            lock (_cachedTables)
            {
                if (!_cachedTables.ContainsKey(typeof(T)))
                    _cachedTables.Add(typeof(T), new T());
            }
        }

        static private T Accessor<T>()
            where T : new()
        {
            if (!_cachedTables.ContainsKey(typeof(T)))
                GetData<T>();
            return (T)_cachedTables[typeof(T)];
        }

        #endregion
    }
}
