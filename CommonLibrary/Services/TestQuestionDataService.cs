﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using CommonLibrary.Interfaces;
using DataModelsLibrary;

namespace CommonLibrary.Services
{
    /// <summary>
    /// "TestQuestions" table services
    /// </summary>
    /// 
    /// <refactor>Serhiy Markevych</refactor>
    /// <date>17 May 2013</date>
    public sealed class TestQuestionDataService : BaseDataService, IDataService<TestQuestionDataModel>
    {
        #region Private Methods

        private void GetEntities(TestQuestion targetData, TestQuestionDataModel modelData)
        {
            if (targetData != null)
            {
                targetData.TestID = modelData.TestID;
                targetData.QuestionID = modelData.QuestionID;
                targetData.OrdinalNumber = modelData.OrdinalNumber;
            }
        }

        private TestQuestion GetEntities(TestQuestionDataModel modelData)
        {
            TestQuestion objectResult = new TestQuestion();
            GetEntities(objectResult, modelData);
            return objectResult;
        }

        #endregion
        /// <summary>
        /// Get record of the table by condition based on a predicate
        /// </summary>
        /// <param name="predicate">Function to test each element for a condition</param>
        /// <returns>Records that matches the search condition</returns>
        public IEnumerable<TestQuestionDataModel> GetData(Expression<Func<TestQuestionDataModel, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                return (from t in context.TestQuestions
                        select new TestQuestionDataModel()
                        {
                            ID = t.ID,
                            TestID = t.TestID,
                            QuestionID = t.QuestionID,
                            OrdinalNumber = t.OrdinalNumber
                        }).Where(predicate).ToArray();
            }
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public bool Add(TestQuestionDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var entity = GetEntities(record);
                context.TestQuestions.AddObject(entity);
                result = context.SaveChanges()>0;
                record.ID = entity.ID;
            }
            return result;
        }

        /// <summary>
        /// Edits record in a table
        /// </summary>
        /// <param name="recordOld">Record to edit</param>
        /// <param name="recordNew">Record with new values</param>
        public bool Edit(TestQuestionDataModel recordOld, TestQuestionDataModel recordNew)
        {
            bool result = false;
            if (recordOld == null || recordNew == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var entity = context.TestQuestions.Where(t => t.ID == recordOld.ID).SingleOrDefault();

                if (entity != null)
                {
                    GetEntities(entity, recordNew);
                    try
                    {
                        result = context.SaveChanges()>0;
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        context.Refresh(RefreshMode.ClientWins, entity);
                        result = context.SaveChanges()>0;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Removes record from a table
        /// </summary>
        /// <param name="record">Record that must be removed</param>
        public bool Remove(TestQuestionDataModel record)
        {
            bool result = false;
            if (record == null)
                throw new ArgumentNullException();

            using (var context = GetContext())
            {
                var entity = context.TestQuestions.Where(t => t.ID == record.ID).SingleOrDefault();

                if (entity != null)
                {
                    context.TestQuestions.DeleteObject(entity);
                    result = context.SaveChanges()>0;
                }
            }
            return result;
        }
    }
}
