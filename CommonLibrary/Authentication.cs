﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using DataModelsLibrary;
using CommonLibrary.Services;

namespace CommonLibrary
{
    /// <summary>
    /// Authentication user in application
    /// </summary>
    /// /// <autor>Evgeny Drapoguz</autor>
    /// <date>16 April 2013</date>
    public static class Authentication
    {
        #region Fields
        private static UserDataModel _currentUser = null;
        #endregion


        #region Property
        public static UserDataModel CurrentUser
        {
            get { return Authentication._currentUser; }
        }
        #endregion


        #region Methods
        public static UserDataModel Login(string username, string password)
        {
            if (Validation.ValidUsername(username) && Validation.ValidPassword(password))
            {
                UserDataService userDataService = new UserDataService();

                UserDataModel selectedUser = userDataService.GetData(u => u.Username == username).FirstOrDefault();

                if (selectedUser != null)
                {
                    if (Authentication.HashPassword(password) == selectedUser.Password)
                    {
                        _currentUser = selectedUser;
                    }
                }
            }

            return _currentUser;
        }

        public static void Logout()
        {
            _currentUser = null;
        }

        public static string HashPassword(string password)
        {
            HashAlgorithm hAlgoritm = MD5.Create();

            byte[] inputBytes = Encoding.UTF8.GetBytes(password);

            byte[] hash = hAlgoritm.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public static bool HasRole(int userId, Role roleName)
        {
            throw new Exception();
        }

        public static bool HasRole(string username, Role roleName)
        {
            throw new Exception();
        }
        #endregion
    }
}
