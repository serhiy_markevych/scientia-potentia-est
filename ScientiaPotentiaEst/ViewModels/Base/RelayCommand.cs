﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.Base
{
    /// <summary>
    /// Base command class.
    /// </summary>
    /// <author>Stolen from msdn magazine by M. Saharilenko.</author>
    /// <link>http://msdn.microsoft.com/ru-ru/magazine/dd419663.aspx</link>
    /// <date>19.04.2013</date>
    public class RelayCommand: ICommand
    {
        #region Fields

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion

        #region Constructors

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            this._execute = execute;
            this._canExecute = canExecute;
        }

        public RelayCommand(Action<object> execute) : this(execute, null)
        { }

        #endregion

        #region ICommand implementation

        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : this._canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            this._execute(parameter);
        }

        #endregion
    }
}
