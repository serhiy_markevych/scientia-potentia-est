﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.Base
{
    /// <summary>
    /// Base view model.
    /// </summary>
    /// <author>Stolen from msdn magazine by M. Saharilenko.</author>
    /// <link>http://msdn.microsoft.com/ru-ru/magazine/dd419663.aspx</link>
    /// <date>19.04.2013</date>
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
                throw new ArgumentException("propertyName", Errors.InvalidPropertyName + propertyName);
        }
    }
}
