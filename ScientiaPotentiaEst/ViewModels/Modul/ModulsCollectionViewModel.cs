﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ScientiaPotentiaEst.ViewModels.Base;
using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.Windows.Modul;

namespace ScientiaPotentiaEst.ViewModels.Modul
{
    /// <summary>
    /// View model for moduls collection.
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public class ModulsCollectionViewModel: BaseViewModel
    {
        #region Fields

        private ObservableCollection<ModulViewModel> _moduls;

        #endregion

        #region Properties

        public ObservableCollection<ModulViewModel> Moduls
        {
            get { return this._moduls; }
            set 
            {
                if (value != null)
                {
                    this._moduls = value;
                    base.OnPropertyChanged("Moduls");
                }
            }
        }
        public ModulViewModel SelectedModul { get; set; }

        #endregion

        #region Commands

        public ICommand AddCommand 
        {
            get { return new RelayCommand(action => this.Add()); }
        }

        public ICommand EditCommand 
        { 
            get { return new RelayCommand(action => this.Edit(), predicate => this.SelectedModul != null); } 
        }

        public ICommand RemoveCommand 
        {
            get { return new RelayCommand(action => this.Remove(), predicate => this.SelectedModul != null); } 
        }

        private void Add()
        {
            ModulActionWindow addWindow = new ModulActionWindow(new ModulViewModel(new ModulDataModel()));
            addWindow.ShowDialog();
            if((bool)addWindow.DialogResult)
                this.ReBinding();
        }

        private void Edit()
        {
            if (this.SelectedModul == null)
            {
                this.Add();
                return;
            }
            ModulActionWindow updWindow = new ModulActionWindow(this.SelectedModul);
            updWindow.ShowDialog();
        }

        private void Remove()
        {
            var ds = DataServicesPool.ModulDataService;
            ds.Remove(this.SelectedModul.GetDataModel());
            this.ReBinding();
        }

        #endregion

        #region Constructors

        public ModulsCollectionViewModel()
        {
            if (!this.ReBinding())
            {
                this.Moduls = new ObservableCollection<ModulViewModel>(new List<ModulViewModel>());
            }
        }

        #endregion

        /// <summary>
        /// ReRead data from database.
        /// </summary>
        /// <returns>Success</returns>
        public bool ReBinding()
        {
            var tempList = DataServicesPool.ModulDataService.GetData(m => true).Select(m => new ModulViewModel(m));
            if (tempList != null)
            {
                this.Moduls = new ObservableCollection<ModulViewModel>(tempList);
                return true;
            }
            return false;
        }
    }
}
