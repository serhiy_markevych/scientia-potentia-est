﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Collections.ObjectModel;
using ScientiaPotentiaEst.ViewModels.Base;
using DataModelsLibrary;
using CommonLibrary.Services;

namespace ScientiaPotentiaEst.ViewModels.Modul
{
    /// <summary>
    /// View model for modul
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public class ModulViewModel: BaseViewModel
    {
        #region Fields

        private ModulDataModel _modulDM;
        private ModulDataModel _tempModulDM;
        private Window _currentWindow;
        private bool _dialogResult;

        #endregion

        #region Properties

        //////Absolute evil//////
        public ObservableCollection<SubjectDataModel> AllSubjectDataModels 
        {
            get { return new ObservableCollection<SubjectDataModel>(DataServicesPool.SubjectDataService.GetData(s => true).Where(s => s.CanceledMark == 0)); }
        }

        public int ID
        { 
            get { return this._modulDM.ID; }
        }

        public string Name 
        {
            get { return this._modulDM.Name; }
            set 
            {
                this._modulDM.Name = value;
                base.OnPropertyChanged("Name");
            }
        }

        public SubjectDataModel Subject
        {
            get { return this._modulDM.Subject; }
            set
            {
                this._modulDM.Subject = value;
                base.OnPropertyChanged("Subject");
            }
        }

        public bool CanceledMark 
        {
            get { return this._modulDM.CanceledMark > 0; }
            set 
            { 
                this._modulDM.CanceledMark = value ? (byte)1 : (byte)0;
                base.OnPropertyChanged("CanceledMark");
            }
        }

        #endregion

        #region Constructors

        public ModulViewModel(ModulDataModel modulDM)
        {
            if (modulDM == null)
                modulDM = new ModulDataModel();
             this._modulDM = modulDM;
             this._tempModulDM = ModulDataModel.CreateFrom(this._modulDM);
        }

        #endregion

        #region Commands

        public ICommand SaveCommand
        {
            get { return new RelayCommand(action => this.Save(), predicate => this._modulDM.IsValid()); }
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(action => this.Cancel()); }
        }

        public void Save()
        {
            if (this._modulDM.Equals(this._tempModulDM))
                return;
            var ds = DataServicesPool.ModulDataService;
            ds.Edit(null, this._modulDM);
            this._dialogResult = true;
            this._tempModulDM.CopyFrom(this._modulDM);
            CloseWindow();
        }

        public void Cancel()
        {
            this._dialogResult = false;
            Rollback();
            CloseWindow();
        }

        #endregion

        #region Other methods

        /// <summary>
        /// Aborting all changes.
        /// </summary>
        private void Rollback()
        {
            if (this._modulDM.Equals(this._tempModulDM))
                return;
            this._modulDM.CopyFrom(this._tempModulDM);
            base.OnPropertyChanged("Name");
            base.OnPropertyChanged("Subject");
            base.OnPropertyChanged("CanceledMark");
        }

        /// <summary>
        /// Setting link to the window which displays this view model.
        /// </summary>
        /// <param name="window"></param>
        public void SetCurrentWindow(Window window)
        {
            if(window != null)
                this._currentWindow = window;
        }

        /// <summary>
        /// Closing window which displays this view model.
        /// </summary>
        private void CloseWindow()
        {
            if (this._currentWindow != null)
            {
                this._currentWindow.DialogResult = this._dialogResult;
                this._currentWindow.Close();
            }
        }

        /// <summary>
        /// Getting data model from this view model.
        /// </summary>
        /// <returns></returns>
        public ModulDataModel GetDataModel()
        {
            return this._modulDM;
        }

        #endregion
    }
}
