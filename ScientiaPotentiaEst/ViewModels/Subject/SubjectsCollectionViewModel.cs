﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows;
using ScientiaPotentiaEst.ViewModels.Base;
using DataModelsLibrary;
using CommonLibrary.Services;
using ScientiaPotentiaEst.Windows.Subject;

namespace ScientiaPotentiaEst.ViewModels.Subject
{
    /// <summary>
    /// View model for subjects collection
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public class SubjectsCollectionViewModel: BaseViewModel
    {
        #region Fields

        private ObservableCollection<SubjectViewModel> _subjects;

        #endregion

        #region Properties

        public ObservableCollection<SubjectViewModel> Subjects 
        {
            get { return this._subjects; }
            set 
            {
                if (value != null)
                {
                    this._subjects = value;
                    base.OnPropertyChanged("Subjects");
                }
            }
        }
        public SubjectViewModel SelectedSubject { get; set; }

        #endregion

        #region Commands

        public ICommand AddCommand 
        {
            get { return new RelayCommand(action => this.Add()); }
        }

        public ICommand EditCommand 
        { 
            get { return new RelayCommand(action => this.Edit(), predicate => this.SelectedSubject != null); } 
        }

        public ICommand RemoveCommand 
        {
            get { return new RelayCommand(action => this.Remove(), predicate => this.SelectedSubject != null); } 
        }

        private void Add()
        {
            SubjectActionWindow addWindow = new SubjectActionWindow(new SubjectViewModel(new SubjectDataModel()));
            addWindow.ShowDialog();
            if((bool)addWindow.DialogResult)
                this.ReBinding();
        }

        private void Edit()
        {
            if (this.SelectedSubject == null)
            {
                this.Add();
                return;
            }
            SubjectActionWindow updWindow = new SubjectActionWindow(this.SelectedSubject);
            updWindow.ShowDialog();
        }

        private void Remove()
        {
            //SubjectDataService.RemoveSubject(this.SelectedSubject.GetDataModel());
            var ds = DataServicesPool.SubjectDataService;
            ds.Remove(this.SelectedSubject.GetDataModel());
            this.ReBinding();
        }

        #endregion

        #region Constructors

        public SubjectsCollectionViewModel()
        {
            if (!this.ReBinding())
            {
                this.Subjects = new ObservableCollection<SubjectViewModel>(new List<SubjectViewModel>());
            }
        }

        #endregion

        /// <summary>
        /// ReRead data from database.
        /// </summary>
        /// <returns>Success</returns>
        public bool ReBinding()
        {
            var tempList = DataServicesPool.SubjectDataService.GetData(s => true).Select(s => new SubjectViewModel(s));
            if (tempList != null)
            {
                this.Subjects = new ObservableCollection<SubjectViewModel>(tempList);
                return true;
            }
            return false;
        }
    }
}
