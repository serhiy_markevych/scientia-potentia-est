﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;
using ScientiaPotentiaEst.ViewModels.Base;
using DataModelsLibrary;
using CommonLibrary.Services;

namespace ScientiaPotentiaEst.ViewModels.Subject
{
    /// <summary>
    /// View model for subject.
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public class SubjectViewModel: BaseViewModel
    {
        #region Fields

        private SubjectDataModel _subjectDM;
        private SubjectDataModel _tempSubjectDM;
        private Window _currentWindow;
        private bool _dialogResult;

        #endregion

        #region Properties

        public int ID
        { 
            get { return this._subjectDM.ID; }
            /*set 
            { 
                this._subjectDM.ID = value;
                base.OnPropertyChanged("ID");
            }*/
        }

        public string Name 
        {
            get { return this._subjectDM.Name; }
            set 
            {
                this._subjectDM.Name = value;
                base.OnPropertyChanged("Name");
            }
        }

        public bool CanceledMark 
        {
            get { return this._subjectDM.CanceledMark > 0; }
            set 
            { 
                this._subjectDM.CanceledMark = value ? (byte)1 : (byte)0;
                base.OnPropertyChanged("CanceledMark");
            }
        }

        #endregion

        #region Constructors

        public SubjectViewModel(SubjectDataModel subjectDM)
        {
            if (subjectDM == null)
                subjectDM = new SubjectDataModel();
             this._subjectDM = subjectDM;
             this._tempSubjectDM = SubjectDataModel.CreateFrom(this._subjectDM);
        }

        #endregion

        #region Commands

        public ICommand SaveCommand
        {
            get { return new RelayCommand(action => this.Save(), predicate => this._subjectDM.IsValid()); }
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(action => this.Cancel()); }
        }

        public void Save()
        {
            if (this._subjectDM.Equals(this._tempSubjectDM))
                return;
            var ds = DataServicesPool.SubjectDataService;
            ds.Edit(null, this._subjectDM);
            this._dialogResult = true;
            this._tempSubjectDM.CopyFrom(this._subjectDM);
            CloseWindow();
        }

        public void Cancel()
        {
            this._dialogResult = false;
            Rollback();
            CloseWindow();
        }

        #endregion

        #region Other methods

        /// <summary>
        /// Aborting all changes.
        /// </summary>
        private void Rollback()
        {
            if (this._subjectDM.Equals(this._tempSubjectDM))
                return;
            this._subjectDM.CopyFrom(this._tempSubjectDM);
            base.OnPropertyChanged("Name");
            base.OnPropertyChanged("CanceledMark");
        }

        /// <summary>
        /// Setting link to the window which displays this view model.
        /// </summary>
        /// <param name="window"></param>
        public void SetCurrentWindow(Window window)
        {
            if(window != null)
                this._currentWindow = window;
        }

        /// <summary>
        /// Closing window which displays this view model.
        /// </summary>
        private void CloseWindow()
        {
            if (this._currentWindow != null)
            {
                this._currentWindow.DialogResult = this._dialogResult;
                this._currentWindow.Close();
            }
        }

        /// <summary>
        /// Getting data model from this view model.
        /// </summary>
        /// <returns></returns>
        public SubjectDataModel GetDataModel()
        {
            return this._subjectDM;
        }

        #endregion
    }
}
