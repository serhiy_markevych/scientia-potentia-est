﻿using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.Profiles
{
    /// <summary>
    /// View Model for profile
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>15.05.2013</date>
    public class ProfileViewModel : BaseViewModel
    {
        #region Fields
        private Window _currentWindow;
        private bool _commandnstatus;
        private ProfilesDataModel _profileDM;
        private ProfilesDataModel _cacheprofileDM;
        #endregion

        #region Properties
        public int ID
        {
            get { return this._profileDM.ID; }
        }

        public string Name
        {
            get { return this._profileDM.Name; }
            set
            {
                this._profileDM.Name = value;
            }
        }

        public int CanceledMark
        {
            get { return this._profileDM.CanceledMark; }
            set
            {
                this._profileDM.CanceledMark = value;
            }
        }

        public int TimeLimit
        {
            get { return this._profileDM.TimeLimit; }
            set
            {
                this._profileDM.TimeLimit = value;
            }
        }

        public int ShuffleQuestions
        {
            get { return this._profileDM.ShuffleQuestions; }
            set
            {
                this._profileDM.ShuffleQuestions = value;
            }
        }

        public int ShuffleAnswers
        {
            get { return this._profileDM.ShuffleAnswers; }
            set
            {
                this._profileDM.ShuffleAnswers = value;
            }
        }

        public int QuestionsCount
        {
            get { return this._profileDM.QuestionsCount; }
            set
            {
                this._profileDM.QuestionsCount = value;
            }
        }

        public ProfileMarkType MarkType
        {
            get { return this._profileDM.MarkType; }
            set
            {
                this._profileDM.MarkType = value;
            }
        }

        public ObservableCollection<ProfileMarkType> ProfileMarkTypes
        {
            get
            {
                List<ProfileMarkType> lmarktypes = new List<ProfileMarkType>();
                Array enumValArray = Enum.GetValues(typeof(ProfileMarkType));

                foreach (var item in enumValArray)
                {
                    lmarktypes.Add((ProfileMarkType)Enum.Parse(typeof(ProfileMarkType), item.ToString()));
                }

                return new ObservableCollection<ProfileMarkType>(lmarktypes);
            }
        }
        #endregion

        #region Constructors
        public ProfileViewModel(ProfilesDataModel profileDM)
        {
            if (profileDM == null)
                profileDM = new ProfilesDataModel();
            this._profileDM = profileDM;
            this._cacheprofileDM = ProfilesDataModel.CreateFrom(this._profileDM);
        }
        #endregion

        #region Commands
        public ICommand SaveCommand
        {
            get { return new RelayCommand(action => this.Save(), predicate => true); }
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(action => this.Cancel()); }
        }

        public void Save()
        {
            if (this._profileDM.Equals(this._cacheprofileDM))
                return;
            var ds = DataServicesPool.ProfileDataService;
            if (this._profileDM.ID > 0) ds.Edit(null, this._profileDM);
            else ds.Add(this._profileDM);
            this._commandnstatus = true;
            this._cacheprofileDM.Copy(this._profileDM);
            CloseWindow();
        }

        public void Cancel()
        {
            this._commandnstatus = false;
            Rollback();
            CloseWindow();
        }
        #endregion

        #region Other methods
        /// <summary>
        /// Rollback changes
        /// </summary>
        private void Rollback()
        {
            if (this._profileDM.Equals(this._cacheprofileDM))
                return;
            this._profileDM.Copy(this._cacheprofileDM);
            base.OnPropertyChanged("Name");
            base.OnPropertyChanged("CanceledMark");
            base.OnPropertyChanged("TimeLimit");
            base.OnPropertyChanged("ShuffleQuestions");
            base.OnPropertyChanged("ShuffleAnswers");
            base.OnPropertyChanged("QuestionsCount");
            base.OnPropertyChanged("MarkType");
        }

        /// <summary>
        /// Set current window
        /// </summary>
        /// <param name="window"></param>
        public void SetCurrentWindow(Window window)
        {
            if (window != null)
                this._currentWindow = window;
        }

        /// <summary>
        /// Closing window which displays this view model.
        /// </summary>
        private void CloseWindow()
        {
            if (this._currentWindow != null)
            {
                this._currentWindow.DialogResult = this._commandnstatus;
                this._currentWindow.Close();
            }
        }

        /// <summary>
        /// Getting data model from this view model.
        /// </summary>
        /// <returns></returns>
        public ProfilesDataModel GetDataModel()
        {
            return this._profileDM;
        }
        #endregion
    }
}
