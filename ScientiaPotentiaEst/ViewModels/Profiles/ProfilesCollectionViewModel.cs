﻿using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Base;
using ScientiaPotentiaEst.Windows.Profile;
using ScientiaPotentiaEst.Windows.RatingScale;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.Profiles
{
    /// <summary>
    /// View Model for profiles collection
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>15.05.2013</date>
    public class ProfilesCollectionViewModel : BaseViewModel
    {
        #region Fields
        private ObservableCollection<ProfileViewModel> _profiles;
        #endregion

        #region Properties
        public ObservableCollection<ProfileViewModel> Profiles
        {
            get { return this._profiles; }
            set
            {
                if (value != null)
                {
                    this._profiles = value;
                    base.OnPropertyChanged("Profiles");
                }
            }
        }
        public ProfileViewModel SelectedProfile { get; set; }
        #endregion

        #region Commands
        public ICommand AddCommand
        {
            get { return new RelayCommand(action => this.Add()); }
        }

        public ICommand EditCommand
        {
            get { return new RelayCommand(action => this.Edit(), predicate => this.SelectedProfile != null); }
        }

        public ICommand RemoveCommand
        {
            get { return new RelayCommand(action => this.Remove(), predicate => this.SelectedProfile != null); }
        }

        public ICommand ShowChild
        {
            get { return new RelayCommand(action => this.Show(), predicate => this.SelectedProfile != null); }
        }

        private void Add()
        {
            ProfileDataWindow dataWindow = new ProfileDataWindow(new ProfileViewModel(new ProfilesDataModel()));
            dataWindow.ShowDialog();
            if ((bool)dataWindow.DialogResult)
                this.ReBinding();
        }

        private void Edit()
        {
            if (this.SelectedProfile != null)
            {
                ProfileDataWindow dataWindow = new ProfileDataWindow(this.SelectedProfile);
                dataWindow.ShowDialog();
                if ((bool)dataWindow.DialogResult)
                    this.ReBinding();
            }
        }

        private void Remove()
        {
            var ds = DataServicesPool.ProfileDataService;
            ds.Remove(this.SelectedProfile.GetDataModel());
            this.ReBinding();
        }

        private void Show()
        {
            if (this.SelectedProfile != null)
            {
                RatingScalesWindow alldataWindow = new RatingScalesWindow(this.SelectedProfile);
                alldataWindow.ShowDialog();
                if ((bool)alldataWindow.DialogResult)
                    this.ReBinding();
            }
        }
        #endregion

        #region Constructors
        public ProfilesCollectionViewModel()
        {
            if (!this.ReBinding())
            {
                this.Profiles = new ObservableCollection<ProfileViewModel>(new List<ProfileViewModel>());
            }
        }
        #endregion

        /// <summary>
        /// ReLoad data from database
        /// </summary>
        /// <returns>Load status</returns>
        public bool ReBinding()
        {
            var data = DataServicesPool.ProfileDataService.GetData(m => true).Select(m => new ProfileViewModel(m));
            if (data != null)
            {
                this.Profiles = new ObservableCollection<ProfileViewModel>(data);
                return true;
            }
            return false;
        }
    }
}
