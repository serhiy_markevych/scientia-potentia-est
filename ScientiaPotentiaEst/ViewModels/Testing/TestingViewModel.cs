﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;

namespace ScientiaPotentiaEst.ViewModels.Testing
{
    /// <summary>
    /// View model test question
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public class TestingViewModel
    {
        public QuestionDataModel Question { get; set; }
        public List<AnswerDataModel> Answers { get; set; }
        public List<TestAnswerDataModel> TestAnswer { get; set; }
        //model with correct answer
        public bool? Correct { get; set; }

        public TestingViewModel()
        { }

        public TestingViewModel(QuestionDataModel question, List<AnswerDataModel> answers)
        {
            if (question == null)
                throw new ArgumentNullException("question");
            if (answers == null)
                throw new ArgumentNullException("answers");

            this.Question = question;
            this.Answers = answers;
        }
    }
}
