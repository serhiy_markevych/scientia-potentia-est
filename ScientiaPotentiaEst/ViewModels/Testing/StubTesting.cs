﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using ResourcesLibrary;
using CommonLibrary.Services;
using CommonLibrary;

namespace ScientiaPotentiaEst.ViewModels.Testing
{
    /// <summary>
    /// Class for loading data for the test
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <review>Bondarenko Evgen</review>
    /// <date>11 May 2013</date>
    public class StubTesting
    {
        private List<TestingViewModel> _testlistTestingViewModel = new List<TestingViewModel>();
        private int curentindex = -1;
        private int _testID; //поле хранит временный ид для метода SaveResult
        public TimeSpan Time;
        public TestMode Mode { get; set; }
        public ModulDataModel CurrentModul { get; set; }
        public ProfilesDataModel CurrentProfile { get; set; }

        public void Complit()
        {
            // формирую класс Тест
            
            TestDataModel test = new TestDataModel();
            //далее заполняю все поля для сохранения
            
            test.Mode = Mode;
            test.ProfileID = 1;// переделать.
            test.StudentID = Authentication.CurrentUser.ID;            
                        
            test.ModulID = CurrentModul.ID;
            test.TotalAnswers = Gettotalanswers();
            test.TotalQuestions = _testlistTestingViewModel.Count();

            test.Rating = 0;
            test.TotalRating = GettotalRating(); 
            test.Date = DateTime.Now; 
            test.Duration = Time;           

            TestDataService testdataservice = new TestDataService();
            testdataservice.Add(test);
            _testID = test.ID;

            Saveresult();
            test.Rating = GetRating();
            testdataservice.Edit(null,test);

        }
        /// <summary>
        /// сохраняет таблицы TestQuestion и TestAnswers
        /// </summary>
        private void Saveresult()
        {
            
            TestQuestionDataService testquestiondataservise = new TestQuestionDataService();
            

            TestAnswerDataService testanswerdataservice = new TestAnswerDataService();
           
            foreach (TestingViewModel m in _testlistTestingViewModel)
            {
                TestQuestionDataModel testquestionDM = new TestQuestionDataModel();
                testquestionDM.TestID = _testID;
                testquestionDM.QuestionID = m.Question.ID;
                testquestionDM.OrdinalNumber = m.Question.OrdinalNumber;
                testquestiondataservise.Add(testquestionDM);
                

                foreach (var n in m.TestAnswer)
                {
                    TestAnswerDataModel testanswerDM = new TestAnswerDataModel();
                    testanswerDM.TestQuestionID = testquestionDM.ID;
                    testanswerDM.OrdinalNumber = n.OrdinalNumber;
                    testanswerDM.Text = n.Text;
                    testanswerDM.AnswerID = n.AnswerID;
                    testanswerdataservice.Add(testanswerDM);
                }

            }

        }
        private int Gettotalanswers()
        {
            int result = 0;
            foreach (TestingViewModel m in _testlistTestingViewModel)
            {
                result += m.TestAnswer.Count();
            }
            return result;
        }
        private int GetRating()
        {
            TestQuestionDataService testquestiondataservise = new TestQuestionDataService();
            var res = testquestiondataservise.GetData(m => m.TestID == _testID).ToList();
            int result = 0;
            StatisticDataService statservice = new StatisticDataService();
            QuestionDataService questiondataservice = new QuestionDataService();
            foreach (TestQuestionDataModel m in res)
            {
                if (statservice.IsCorrectAnswer(m.ID))
                {
                   result += questiondataservice.GetData(o=>o.ID==m.QuestionID).Select(o=>o.Significance).FirstOrDefault();
                }
            }

            return result;
        }
        private int GettotalRating()
        {
            int result = 0;
            foreach (TestingViewModel m in _testlistTestingViewModel)
            {
                result += m.Question.Significance;
            }
            return result;
        }
        public TestingViewModel NextQuestion()
        {
            if (curentindex < _testlistTestingViewModel.Count - 1) curentindex++;
            return _testlistTestingViewModel[curentindex];
        }
        public TestingViewModel PrevQuestion()
        {
            if (curentindex > 0) curentindex--;
            return _testlistTestingViewModel[curentindex];
        }

        public TestingViewModel CorrectCurentAnswer()
        {
            TestingViewModel temp = new TestingViewModel();
            temp.Correct = true;
            temp.Question = new QuestionDataModel { Text = DisplayLabels.CorrectAnswersLabel };
            if ((0 <= curentindex) && (curentindex < _testlistTestingViewModel.Count))
            {
                if ((_testlistTestingViewModel[curentindex].Question.Type == QuestionType.OneAnswer) ||
                    (_testlistTestingViewModel[curentindex].Question.Type == QuestionType.MultipleAnswers))
                {
                    temp.Answers = _testlistTestingViewModel[curentindex].Answers
                        .Select(sel => new AnswerDataModel
                                {
                                    Text = sel.Text,
                                    Significance = sel.Significance,
                                    OrdinalNumber = sel.OrdinalNumber,
                                    Group = sel.Group,
                                    IsChecked = sel.Significance != 0
                                }).ToList<AnswerDataModel>();
                }
                if (_testlistTestingViewModel[curentindex].Question.Type == QuestionType.TypeAnswer)
                {
                    temp.TestAnswer = new List<TestAnswerDataModel>();
                    temp.TestAnswer.Add(new TestAnswerDataModel { Text = _testlistTestingViewModel[curentindex].Answers[0].Text });
                }
                if ((_testlistTestingViewModel[curentindex].Question.Type == QuestionType.Compliance) ||
                    (_testlistTestingViewModel[curentindex].Question.Type == QuestionType.Ordering) ||
                    (_testlistTestingViewModel[curentindex].Question.Type == QuestionType.Classification))
                {                   
                    temp.Answers = _testlistTestingViewModel[curentindex].Answers;
                }
            }
            return temp;
        }

        public StubTesting(TestMode mode, ModulDataModel currentModul)
        {
            if (currentModul == null)
                throw new ArgumentNullException("currentModul");

            CurrentModul = currentModul;
            Mode = mode;

            if (Authentication.CurrentUser != null)
            {
                AssignDataService assignService = new AssignDataService();
                int profileID = assignService.GetData(o => o.Group.ID == Authentication.CurrentUser.Group.ID && o.Module.ID == CurrentModul.ID)
                    .Select(o => o.Profile.ID).FirstOrDefault();

                ProfileDataService profileService = new ProfileDataService();
                CurrentProfile = profileService.GetData(o=>o.ID == profileID).FirstOrDefault();
            }

            if (CurrentProfile == null)
                throw new NullReferenceException();

            Time = new TimeSpan(0, 1, 0);

            QuestionDataService questionService = new QuestionDataService();
            List<QuestionDataModel> questions = ShuffleQuestions(questionService.GetData(o => o.ModulID == CurrentModul.ID && o.CanceledMark == 0)
                .Take(CurrentProfile.QuestionsCount).ToList(), CurrentProfile.ShuffleQuestions != 0);

            List<int> questionCodes = questions.Select(o => o.ID).ToList();
            AnswerDataService answerService = new AnswerDataService();
            List<AnswerDataModel> answers = answerService.GetData(o => questionCodes.Contains(o.QuestionID) && o.CanceledMark == 0).ToList();

            foreach (var question in questions)
                _testlistTestingViewModel.Add(new TestingViewModel(question, 
                    ShuffleAnswers(answers.Where(o => o.QuestionID == question.ID).ToList(), CurrentProfile.ShuffleAnswers != 0)));
        }

        private List<QuestionDataModel> ShuffleQuestions(List<QuestionDataModel> list, bool shuffle)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            if (shuffle)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int ordinalNumber = 1;
                foreach (var item in list.OrderBy(o => rand.Next()))
                    item.OrdinalNumber = ordinalNumber++;
            }

            return list.OrderBy(o => o.OrdinalNumber).ToList();
        }

        private List<AnswerDataModel> ShuffleAnswers(List<AnswerDataModel> list, bool shuffle)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            if (shuffle)
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int ordinalNumber = 1;
                foreach (var item in list.OrderBy(o => rand.Next()))
                    item.OrdinalNumber = ordinalNumber++;
            }

            return list.OrderBy(o => o.OrdinalNumber).ToList();
        }
    }
}
