﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.Testing.UIQuestionViewModel
{
    /// <summary>
    /// View model UserControl test question for ordering
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public class OrderingHandlerViewModel
    {
        #region field and property
        private TestingViewModel _model;
        public string Textquestion { get { return _model.Question.Text; } }
        public List<AnswerDataModel> AnswerDinamic { get; set; }
        #endregion

        #region constructor
        public OrderingHandlerViewModel(TestingViewModel model)
        {
            _model = model;
            if (_model.Correct == true) InitCorrect();
            else InitTesting();                     
        }

        private void InitCorrect()
        {
            AnswerDinamic = _model.Answers.OrderBy(o => o.Significance).ToList<AnswerDataModel>();
        }

        private void InitTesting()
        {
            if (_model.TestAnswer == null)
            {
                _model.TestAnswer = (from a in _model.Answers
                                     select new TestAnswerDataModel { AnswerID = a.ID, OrdinalNumber = a.OrdinalNumber })
                                    .ToList<TestAnswerDataModel>();
                                    
            }
            AnswerDinamic = _model.TestAnswer.OrderBy(or => or.OrdinalNumber).Select(sel => new AnswerDataModel
            {
                ID = sel.AnswerID ?? 0,
                Text = _model.Answers.FirstOrDefault(w => w.ID == sel.AnswerID).Text
            }).ToList<AnswerDataModel>();
        }
        #endregion

        #region public method
        public void Up(AnswerDataModel answer)
        {
            if (answer != null)
            {
                int curent = AnswerDinamic.IndexOf(answer);
                if (curent > 0)
                {
                    AnswerDinamic.RemoveAt(curent);
                    AnswerDinamic.Insert(curent - 1, answer);

                    TestAnswerDataModel testanswercurent = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == answer.ID);
                    TestAnswerDataModel testanswerprev = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == AnswerDinamic[curent].ID);
                    int tempordinalnumber = testanswercurent.OrdinalNumber;
                    testanswercurent.OrdinalNumber = testanswerprev.OrdinalNumber;
                    testanswerprev.OrdinalNumber = tempordinalnumber;                   
                }
            }
        }

        public void Down(AnswerDataModel answer)
        {                 
            if (answer != null)
            {
                int max = AnswerDinamic.Count - 1;
                int curent = AnswerDinamic.IndexOf(answer);
                if (curent < max)
                {
                    AnswerDinamic.RemoveAt(curent);
                    AnswerDinamic.Insert(curent + 1, answer);
                    TestAnswerDataModel testanswercurent = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == answer.ID);
                    TestAnswerDataModel testanswerprev = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == AnswerDinamic[curent].ID);
                    int tempordinalnumber = testanswercurent.OrdinalNumber;
                    testanswercurent.OrdinalNumber = testanswerprev.OrdinalNumber;
                    testanswerprev.OrdinalNumber = tempordinalnumber;  
                }
            }
        }
        #endregion
    }
}
