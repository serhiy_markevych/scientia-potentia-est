﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.Testing.UIQuestionViewModel
{
    /// <summary>
    /// View model UserControl test question for classification
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public class ClassificationHandlerViewModel
    {
        #region field and property
        private TestingViewModel _model;
        public List<AnswerDataModel> Answer { get; set; }
        public List<String> Group { get; set; }
        public List<AnswerDataModel> GroupingAnswer { get; set; }
        public string Textquestion { get { return _model.Question.Text; } }
        #endregion

        #region constructor
        public ClassificationHandlerViewModel(TestingViewModel model)
        {
            _model = model;
            if (_model.Correct == true) InitCorrect();
            else InitTesting();           
        }

        private void InitTesting()
        {
            if (_model.TestAnswer == null)
            {
                _model.TestAnswer = new List<TestAnswerDataModel>();
                for (int j = 0; j < _model.Answers.Count; j++)
                    _model.TestAnswer.Add(new TestAnswerDataModel());
            }
            Answer = _model.Answers.Select(sel => new AnswerDataModel { Text = sel.Text, ID = sel.ID }).ToList<AnswerDataModel>();
            Group = _model.Answers.Select(sel => sel.Group).Distinct().ToList<String>();
            GroupingAnswer = _model.TestAnswer.Where(w => w.Text != "").Select(sel => new AnswerDataModel
            {
                ID = sel.AnswerID ?? 0,
                Text = Answer.FirstOrDefault(w => w.ID == sel.AnswerID).Text,
                Group = sel.Text
            }).ToList<AnswerDataModel>();
            Answer.RemoveAll(w => GroupingAnswer.Count(gw => (w.ID == gw.ID) && gw.Group != "") == 1);
        }

        private void InitCorrect()
        {
            Answer = _model.Answers.Select(sel => new AnswerDataModel { Text = sel.Text, ID = sel.ID }).ToList<AnswerDataModel>();
            Group = _model.Answers.Select(sel => sel.Group).Distinct().ToList<String>();
            GroupingAnswer = _model.Answers;
        }
        #endregion

        #region public method
        public void Grouping(AnswerDataModel answer, string group)
        {
            Answer.Remove(answer);
            answer.Group = group;
            GroupingAnswer.Add(answer);
            TestAnswerDataModel temp = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == answer.ID);
            temp.Text = group;
        }

        public void DeGrouping(AnswerDataModel answer)
        {
            GroupingAnswer.Remove(answer);
            answer.Group = "";
            Answer.Add(answer);
            TestAnswerDataModel temp = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == answer.ID);
            temp.Text = "" ;
        }
        #endregion
    }
}
