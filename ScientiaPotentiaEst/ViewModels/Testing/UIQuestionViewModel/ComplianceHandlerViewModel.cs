﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.Testing.UIQuestionViewModel
{
    /// <summary>
    /// View model UserControl test question for compliance
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public class ComplianceHandlerViewModel
    {
        #region field and property
        private TestingViewModel _model;
        private List<AnswerDataModel> _answersatatic;
        private List<AnswerDataModel> _answerdinamic;

        public string Textquestion { get { return _model.Question.Text; } }
        public List<AnswerDataModel> AnswerSatatic { get { return _answersatatic; } }
        public List<AnswerDataModel> AnswerDinamic { get { return _answerdinamic; } }
        #endregion

        #region constructor
        public ComplianceHandlerViewModel(TestingViewModel model)
        {
            _model = model;
            _answersatatic=new List<AnswerDataModel>();
            _answerdinamic = new List<AnswerDataModel>();
            if (_model.Correct == true) InitCorrect();
            else InitTesting();
        }

        private void InitTesting()
        {
            if (_model.TestAnswer == null)
            {
                _model.TestAnswer = new List<TestAnswerDataModel>();               
                for (int j = 0; j < _model.Answers.Count; j++)
                    _model.TestAnswer.Add(new TestAnswerDataModel());
            }
            int i = 0;
            foreach (var item in _model.Answers)
            {
                if (_answersatatic.Count(w => w.Significance == item.Significance) == 0)
                {
                    _answersatatic.Add(item);
                    _model.TestAnswer[i].AnswerID = item.ID;
                    _model.TestAnswer[i++].OrdinalNumber = item.Significance;
                }
                else
                {
                    _answerdinamic.Add(item);
                    _model.TestAnswer[i].AnswerID = item.ID;
                    _model.TestAnswer[i++].OrdinalNumber = _answersatatic[_answerdinamic.Count - 1].Significance;
                }
            }
        }

        private void InitCorrect()
        {
            
            IEnumerable<AnswerDataModel> tempanswer = _model.Answers;
            while (tempanswer.Count() != 0)
            {
                int min = tempanswer.Min(m => m.Significance);
                List<AnswerDataModel> temp = _model.Answers.Where(w => w.Significance == min).ToList<AnswerDataModel>();
                _answersatatic.Add(temp[0]);
                _answerdinamic.Add(temp[1]);
                tempanswer = _model.Answers.Where(w => w.Significance > min);
            };
        }
        #endregion

        #region public method
        public void Up(AnswerDataModel temp)
        {
            if (temp != null)
            {
                int curent = _answerdinamic.IndexOf(temp);
                if (curent > 0)
                {
                    _answerdinamic.RemoveAt(curent);
                    _answerdinamic.Insert(curent - 1, temp);
                    TestAnswerDataModel testanswercurent = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == temp.ID);
                    TestAnswerDataModel testanswerprev = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == _answerdinamic[curent].ID);
                    int tempordinalnumber = testanswercurent.OrdinalNumber;
                    testanswercurent.OrdinalNumber = testanswerprev.OrdinalNumber;
                    testanswerprev.OrdinalNumber = tempordinalnumber;                  
                }
            }
        }
        public void Down(AnswerDataModel temp)
        {                 
            if (temp != null)
            {
                int max = _answerdinamic.Count - 1;
                int curent = _answerdinamic.IndexOf(temp);
                if (curent < max)
                {
                    _answerdinamic.RemoveAt(curent);
                    _answerdinamic.Insert(curent + 1, temp);
                    TestAnswerDataModel testanswercurent = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == temp.ID);
                    TestAnswerDataModel testanswerprev = _model.TestAnswer.FirstOrDefault(w => w.AnswerID == _answerdinamic[curent].ID);
                    int tempordinalnumber = testanswercurent.OrdinalNumber;
                    testanswercurent.OrdinalNumber = testanswerprev.OrdinalNumber;
                    testanswerprev.OrdinalNumber = tempordinalnumber;  
                }
            }
        }
        #endregion
    }
}
