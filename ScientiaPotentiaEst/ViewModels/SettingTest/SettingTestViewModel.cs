﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using CommonLibrary;
using CommonLibrary.Services;
using ScientiaPotentiaEst.ViewModels.Base;

namespace ScientiaPotentiaEst.ViewModels.SubjectTesting
{
    /// <summary>
    /// View model test subjects and modes
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    /// <refactor>Dmitriy Zhemkov</refactor>
    /// <date>27.05.2013</date>
    public class SettingTestViewModel : BaseViewModel
    {
        public List<ModulDataModel> Moduls { get; set; }
        public ModulDataModel SelectedModul { get; set; }
        private TestMode _mode;
        public TestMode Mode 
        {
            get { return _mode; }
            set
            {
                if (_mode != value)
                {
                    _mode = value;
                    OnPropertyChanged("IsLearningMode");
                    OnPropertyChanged("IsTestingMode");
                }
            }
        }

        public SettingTestViewModel()
        {
            if (Authentication.CurrentUser != null)
            {
                //Choose all module IDs for the current student group
                AssignDataService assignService = new AssignDataService();
                List<int> modulCodes = assignService.GetData(o => o.Group.ID == Authentication.CurrentUser.Group.ID).Select(o => o.Module.ID).ToList();

                //Choose all module IDs that has at least one uncanceled question
                QuestionDataService questionService = new QuestionDataService();
                List<int> modulCodesWithQuestions = questionService.GetData(o => modulCodes.Contains(o.ModulID) && o.CanceledMark == 0).Select(o => o.ModulID).Distinct().ToList();

                if (modulCodesWithQuestions != null)
                {
                    ModulDataService modulService = new ModulDataService();
                    Moduls = modulService.GetData(o => modulCodesWithQuestions.Contains(o.ID) && o.CanceledMark == 0).OrderBy(o => o.Name).ToList();
                }
            }

            if (Moduls == null)
            {
                Moduls = new List<ModulDataModel>();
                SelectedModul = null;
            }
            else
                SelectedModul = Moduls.FirstOrDefault();

            Mode = TestMode.Learning;
        }

        public bool IsLearningMode
        {
            get { return Mode == TestMode.Learning; }
            set { Mode = value ? TestMode.Learning : Mode; }
        }

        public bool IsTestingMode
        {
            get { return Mode == TestMode.Testing; }
            set { Mode = value ? TestMode.Testing : Mode; }
        }
    }
}
