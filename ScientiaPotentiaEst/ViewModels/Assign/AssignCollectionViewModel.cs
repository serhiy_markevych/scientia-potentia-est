﻿using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Base;
using ScientiaPotentiaEst.Windows.Assign;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.Assign
{
    /// <summary>
    /// View Model for assign collection
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>17.05.2013</date>
    public class AssignCollectionViewModel : BaseViewModel
    {
        #region Fields
            private ObservableCollection<AssignViewModel> _assigns;
        #endregion

        #region Properties
            public ObservableCollection<AssignViewModel> Assigns
            {
                get { return this._assigns; }
                set 
                {
                    if (value != null)
                    {
                        this._assigns = value;
                        base.OnPropertyChanged("Assigns");
                    }
                }
            }

            public AssignViewModel SelectedAssign { get; set; }
        #endregion

        #region Commands
            public ICommand AddCommand 
            {
                get { return new RelayCommand(action => this.Add()); }
            }

            public ICommand EditCommand 
            { 
                get { return new RelayCommand(action => this.Edit(), predicate => this.SelectedAssign != null); } 
            }

            public ICommand RemoveCommand 
            {
                get { return new RelayCommand(action => this.Remove(), predicate => this.SelectedAssign != null); } 
            }

            private void Add()
            {
                AssignDataWindow dataWindow = new AssignDataWindow(new AssignViewModel(new AssignDataModel()));
                dataWindow.ShowDialog();
                if((bool)dataWindow.DialogResult)
                    this.ReBinding();
            }

            private void Edit()
            {
                if (this.SelectedAssign != null)
                {
                    AssignDataWindow dataWindow = new AssignDataWindow(this.SelectedAssign);
                    dataWindow.ShowDialog();
                    if ((bool)dataWindow.DialogResult)
                        this.ReBinding();
                }
            }

            private void Remove()
            {
                var ds = DataServicesPool.AssignDataService;
                ds.Remove(this.SelectedAssign.GetDataModel());
                this.ReBinding();
            }
        #endregion

        #region Constructors
            public AssignCollectionViewModel()
            {
                if (!this.ReBinding())
                {
                    this.Assigns = new ObservableCollection<AssignViewModel>(new List<AssignViewModel>());
                }
            }

            public AssignCollectionViewModel(AssignViewModel profile)
            {
                if (!this.ReBinding())
                {
                    this.Assigns = new ObservableCollection<AssignViewModel>(new List<AssignViewModel>());
                }
            }
        #endregion

        /// <summary>
        /// ReLoad data from database
        /// </summary>
        /// <returns>Load status</returns>
        public bool ReBinding()
        {
            var data = DataServicesPool.AssignDataService.GetData(m => true).Select(m => new AssignViewModel(m));
            if (data != null)
            {
                this.Assigns = new ObservableCollection<AssignViewModel>(data);
                return true;
            }
            return false;
        }
    }
}
