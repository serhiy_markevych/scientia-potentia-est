﻿using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.Assign
{
    /// <summary>
    /// View Model for assign
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>17.05.2013</date>
    public class AssignViewModel : BaseViewModel
    {
        #region Fields
            private Window _currentWindow;
            private bool _commandnstatus;
            private AssignDataModel _assignDM;
            private AssignDataModel _cacheassignDM;
        #endregion

        #region Properties
            public int ID
            { 
                get { return this._assignDM.ID; }
            }

            public GroupDataModel Group 
            {
                get { return this._assignDM.Group; }
                set 
                {
                    this._assignDM.Group = value;
                }
            }

            public UserDataModel Teacher 
            {
                get { return this._assignDM.Teacher; }
                set 
                {
                    this._assignDM.Teacher = value;
                }
            }

            public ProfilesDataModel Profile 
                {
                    get { return this._assignDM.Profile; }
                    set 
                    {
                        this._assignDM.Profile = value;
                    }
                }

            public ModulDataModel Module 
                {
                    get { return this._assignDM.Module; }
                    set 
                    {
                        this._assignDM.Module = value;
                    }
                }

            public DateTime? TestDate 
                {
                    get { return this._assignDM.TestDate; }
                    set 
                    {
                        this._assignDM.TestDate = value;
                    }
                }

            public ObservableCollection<GroupDataModel> GroupsData
            {
                get { return new ObservableCollection<GroupDataModel>(DataServicesPool.GroupDataService.GetData(g => true).Where(g => g.CanceledMark == 0)); }
            }

            public ObservableCollection<ProfilesDataModel> ProfilesData
            {
                get { return new ObservableCollection<ProfilesDataModel>(DataServicesPool.ProfileDataService.GetData(p => true)); }
            }

            public ObservableCollection<UserDataModel> TeachersData
            {
                get { return new ObservableCollection<UserDataModel>(DataServicesPool.UserDataService.GetData(t => true).Where(t => t.CanceledMark == 0).Where(t => t.HasRole(UserRole.Teacher))); }
            }

            public ObservableCollection<ModulDataModel> ModulsData
            {
                get { return new ObservableCollection<ModulDataModel>(DataServicesPool.ModulDataService.GetData(m => true).Where(m => m.CanceledMark == 0)); }
            }
        #endregion

        #region Constructors
            public AssignViewModel(AssignDataModel assignDM)
            {
                if (assignDM == null)
                    assignDM = new AssignDataModel();
                this._assignDM = assignDM;
                this._cacheassignDM = AssignDataModel.CreateFrom(this._assignDM);
            }
        #endregion

        #region Commands
            public ICommand SaveCommand
            {
                get { return new RelayCommand(action => this.Save(), predicate => true); }
            }

            public ICommand CancelCommand
            {
                get { return new RelayCommand(action => this.Cancel()); }
            }

            public void Save()
            {
                if (this._assignDM.Equals(this._cacheassignDM)) 
                    return;
                var ds = DataServicesPool.AssignDataService;
                if (this._assignDM.ID > 0) ds.Edit(null, this._assignDM);
                else ds.Add(this._assignDM);
                this._commandnstatus = true;
                this._cacheassignDM.Copy(this._assignDM);
                CloseWindow();
            }

            public void Cancel()
            {
                this._commandnstatus = false;
                Rollback();
                CloseWindow();
            }
        #endregion

        #region Other methods
            /// <summary>
            /// Rollback changes
            /// </summary>
            private void Rollback()
            {
                if (this._assignDM.Equals(this._cacheassignDM))
                    return;
                this._assignDM.Copy(this._cacheassignDM);
                base.OnPropertyChanged("Group");
                base.OnPropertyChanged("Teacher");
                base.OnPropertyChanged("Profile");
                base.OnPropertyChanged("Module");
                base.OnPropertyChanged("TestDate");
            }

            /// <summary>
            /// Set current window
            /// </summary>
            /// <param name="window"></param>
            public void SetCurrentWindow(Window window)
            {
                if(window != null)
                    this._currentWindow = window;
            }

            /// <summary>
            /// Closing window which displays this view model.
            /// </summary>
            private void CloseWindow()
            {
                if (this._currentWindow != null)
                {
                    this._currentWindow.DialogResult = this._commandnstatus;
                    this._currentWindow.Close();
                }
            }

            /// <summary>
            /// Getting data model from this view model.
            /// </summary>
            /// <returns></returns>
            public AssignDataModel GetDataModel()
            {
                return this._assignDM;
            }
        #endregion
    }
}
