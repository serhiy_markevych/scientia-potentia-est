﻿using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.RatingScales
{
    /// <summary>
    /// View Model for rating scales
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>17.05.2013</date>
    public class RatingScaleViewModel : BaseViewModel
    {
        #region Fields
            private Window _currentWindow;
            private bool _commandnstatus;
            private RatingScaleDataModel _ratingscaleDM;
            private RatingScaleDataModel _cacheratingscaleDM;
        #endregion

        #region Properties
            public int ID
            { 
                get { return this._ratingscaleDM.ID; }
            }

            public ProfilesDataModel Profile 
            {
                get { return this._ratingscaleDM.Profile; }
                set 
                {
                    this._ratingscaleDM.Profile = value;
                }
            }

            public string Text
            {
                get { return this._ratingscaleDM.Text; }
                set
                {
                    this._ratingscaleDM.Text = value;
                }
            }

            public decimal LowBound
            {
                get { return this._ratingscaleDM.LowBound; }
                set
                {
                    this._ratingscaleDM.LowBound = value;
                }
            }
        #endregion

        #region Constructors
            public RatingScaleViewModel(RatingScaleDataModel ratingscaleDM)
            {
                if (ratingscaleDM == null)
                    ratingscaleDM = new RatingScaleDataModel();
                this._ratingscaleDM = ratingscaleDM;
                this._cacheratingscaleDM = RatingScaleDataModel.CreateFrom(this._ratingscaleDM);
            }
        #endregion

        #region Commands
            public ICommand SaveCommand
            {
                get { return new RelayCommand(action => this.Save(), predicate => true); }
            }

            public ICommand CancelCommand
            {
                get { return new RelayCommand(action => this.Cancel()); }
            }

            public void Save()
            {
                if (this._ratingscaleDM.Equals(this._cacheratingscaleDM)) 
                    return;
                var ds = DataServicesPool.RatingScaleDataService;
                if (this._ratingscaleDM.ID > 0) ds.Edit(null, this._ratingscaleDM);
                else ds.Add(this._ratingscaleDM);
                this._commandnstatus = true;
                this._cacheratingscaleDM.Copy(this._ratingscaleDM);
                CloseWindow();
            }

            public void Cancel()
            {
                this._commandnstatus = false;
                Rollback();
                CloseWindow();
            }
        #endregion

        #region Other methods
            /// <summary>
            /// Rollback changes
            /// </summary>
            private void Rollback()
            {
                if (this._ratingscaleDM.Equals(this._cacheratingscaleDM))
                    return;
                this._ratingscaleDM.Copy(this._cacheratingscaleDM);
                /*base.OnPropertyChanged("Profile");*/
                base.OnPropertyChanged("Text");
                base.OnPropertyChanged("LowBound");
             }

            /// <summary>
            /// Set current window
            /// </summary>
            /// <param name="window"></param>
            public void SetCurrentWindow(Window window)
            {
                if(window != null)
                    this._currentWindow = window;
            }

            /// <summary>
            /// Closing window which displays this view model.
            /// </summary>
            private void CloseWindow()
            {
                if (this._currentWindow != null)
                {
                    this._currentWindow.DialogResult = this._commandnstatus;
                    this._currentWindow.Close();
                }
            }

            /// <summary>
            /// Getting data model from this view model.
            /// </summary>
            /// <returns></returns>
            public RatingScaleDataModel GetDataModel()
            {
                return this._ratingscaleDM;
            }
        #endregion
    }
}
