﻿using CommonLibrary.Services;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Base;
using ScientiaPotentiaEst.ViewModels.Profiles;
using ScientiaPotentiaEst.Windows.RatingScale;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ScientiaPotentiaEst.ViewModels.RatingScales
{
    /// <summary>
    /// View Model for ratitn scales collection
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>15.05.2013</date>
    public class RatingScalesCollectionViewModel : BaseViewModel
    {
        #region Fields
            private ObservableCollection<RatingScaleViewModel> _ratingscales;
        #endregion

        #region Properties
            public ObservableCollection<RatingScaleViewModel> RatingScales
            {
                get { return this._ratingscales; }
                set 
                {
                    if (value != null)
                    {
                        this._ratingscales = value;
                        base.OnPropertyChanged("RatingScales");
                    }
                }
            }

            public RatingScaleViewModel SelectedRatingScale { get; set; }

            public ProfileViewModel CurrentProfile { get; set; }
        #endregion

        #region Commands
            public ICommand AddCommand 
            {
                get { return new RelayCommand(action => this.Add()); }
            }

            public ICommand EditCommand 
            { 
                get { return new RelayCommand(action => this.Edit(), predicate => this.SelectedRatingScale != null); } 
            }

            public ICommand RemoveCommand 
            {
                get { return new RelayCommand(action => this.Remove(), predicate => this.SelectedRatingScale != null); } 
            }

            private void Add()
            {
                RatingScaleDataWindow dataWindow = new RatingScaleDataWindow(new RatingScaleViewModel(new RatingScaleDataModel()), CurrentProfile);
                dataWindow.ShowDialog();
                if((bool)dataWindow.DialogResult)
                    this.ReBinding(CurrentProfile);
            }

            private void Edit()
            {
                if (this.SelectedRatingScale != null)
                {
                    RatingScaleDataWindow dataWindow = new RatingScaleDataWindow(this.SelectedRatingScale, CurrentProfile);
                    dataWindow.ShowDialog();
                    if ((bool)dataWindow.DialogResult)
                        this.ReBinding(CurrentProfile);
                }
            }

            private void Remove()
            {
                var ds = DataServicesPool.RatingScaleDataService;
                ds.Remove(this.SelectedRatingScale.GetDataModel());
                this.ReBinding(CurrentProfile);
            }
        #endregion

        #region Constructors
            public RatingScalesCollectionViewModel()
            {
                if (!this.ReBinding())
                {
                    this.RatingScales = new ObservableCollection<RatingScaleViewModel>(new List<RatingScaleViewModel>());
                }
            }

            public RatingScalesCollectionViewModel(ProfileViewModel profile)
            {
                CurrentProfile = profile;
                if (!this.ReBinding(profile))
                {
                    this.RatingScales = new ObservableCollection<RatingScaleViewModel>(new List<RatingScaleViewModel>());
                }
            }
        #endregion

        /// <summary>
        /// ReLoad data from database
        /// </summary>
        /// <returns>Load status</returns>
        public bool ReBinding()
        {
            var data = DataServicesPool.RatingScaleDataService.GetData(m => true).Select(m => new RatingScaleViewModel(m));
            if (data != null)
            {
                this.RatingScales = new ObservableCollection<RatingScaleViewModel>(data);
                return true;
            }
            return false;
        }

        /// <summary>
        /// ReLoad data from database for current profile
        /// </summary>
        /// <returns>Load status</returns>
        public bool ReBinding(ProfileViewModel profile)
        {
            var data = DataServicesPool.RatingScaleDataService.GetData(m => true).Select(m => new RatingScaleViewModel(m)).Where(m => m.Profile.ID == profile.ID);
            if (data != null)
            {
                this.RatingScales = new ObservableCollection<RatingScaleViewModel>(data);
                return true;
            }
            return false;
        }
    }
}
