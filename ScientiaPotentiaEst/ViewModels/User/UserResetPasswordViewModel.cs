﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using System.ComponentModel;
using CommonLibrary;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.User
{
    /// <summary>
    /// ViewModel for user authenticaton window
    /// </summary>
    /// <author>Evgeny Drapoguz</author>
    /// <date>18 April 2013</date>
    class UserResetPasswordViewModel : IDataErrorInfo
    {
        #region Fields
        private bool _firstRun;
        private string _username;
        private string _secretCode;
        private UserDataModel _oldUserData;
        private string _newPassword;
        #endregion


        #region Property
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                _firstRun = false;
            }
        }

        public string SecretCode
        {
            get { return _secretCode; }
            set
            {
                _secretCode = value;
                _firstRun = false;
            }
        }

        public UserDataModel OldUserData
        {
            get { return _oldUserData; }
            set
            {
                _oldUserData = value;
                _firstRun = false;
            }
        }

        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                _newPassword = value;
                _firstRun = false;
            }
        }
        #endregion


        #region Constructors
        public UserResetPasswordViewModel()
        {
            _firstRun = true;
            _username = "";
            _secretCode = "";
            _oldUserData = null;
            _newPassword = "";
        }
        #endregion


        #region Implement IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string result = "";

                switch (columnName)
                {
                    case "Username": if (!_firstRun && !Validation.ValidUsername(Username)) result = Messages.UserNotValid; break;
                    case "SecretCode": if (!_firstRun && String.IsNullOrEmpty(SecretCode)) result = Messages.CodeNotValid; break;
                    case "NewPassword": if (!_firstRun && !Validation.ValidPassword(NewPassword)) result = Messages.NewPasswordNotValid; break;
                };

                return result;
            }
        }
        #endregion

        #region Methods
        public bool IsValid()
        {
            bool valid = true;
            _firstRun = false;

            if (this["Username"] != String.Empty) valid = false;
            if (this["SecretCode"] != String.Empty) valid = false;
            if (this["NewPassword"] != String.Empty) valid = false;

            return valid;
        }
        #endregion
    }
}
