﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using System.ComponentModel;
using CommonLibrary;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.User
{
    /// <summary>
    /// ViewModel for user authenticaton window
    /// </summary>
    /// <author>Evgeny Drapoguz</author>
    /// <date>16 April 2013</date>
    public class UserAuthenticationViewModel : IDataErrorInfo
    {
        #region Fields
        private bool _firstRun;
        private string _username;
        private string _password;
        #endregion


        #region Property
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                _firstRun = false;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                _firstRun = false;
            }
        }
        #endregion


        #region Constructors
        public UserAuthenticationViewModel()
        {
            _firstRun = true;
            _username = "";
            _password = "";
        }

        public UserAuthenticationViewModel(string username, string password)
        {
            _firstRun = true;
            _username = username;
            _password = password;

        }
        #endregion


        #region Implement IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string result = "";

                switch (columnName)
                {
                    case "Username": if (!_firstRun && !Validation.ValidUsername(Username)) result = Messages.UserNotValid; break;
                    case "Password": if (!_firstRun && !Validation.ValidPassword(Password)) result = Messages.PasswordNotValid; break;
                    default: break;
                };

                return result;
            }
        }
        #endregion


        #region Methods
        public bool IsValid()
        {
            bool valid = true;
            _firstRun = false;

            if (this["Username"] != String.Empty) valid = false;
            if (this["Password"] != String.Empty) valid = false;

            return valid;
        }
        #endregion
    }
}
