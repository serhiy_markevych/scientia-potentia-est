﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;

namespace ScientiaPotentiaEst.ViewModels.Statistic
{
    /// <summary>
    /// ViewModel for user statistic
    /// </summary>
    /// <autor>Cherednychok Ivan</autor>
    /// <date>11 May 2013</date>
    public class StatisticTeacherViewModel
    {
        public UserStatisticDataModel SelectedStatistic { get; set; }
        public List<UserStatisticDataModel> Statistics { get; set; }

        public StatisticTeacherViewModel()
        {
            Statistics = new List<UserStatisticDataModel>();
            SelectedStatistic = new UserStatisticDataModel();

            #region Fake Data
            //TODO: Remove fake data
            
            UserStatisticDataModel dm1 = new UserStatisticDataModel();
            dm1.Subject = "Algebra";

            TestInfo test1 = new TestInfo();
            test1.AuthorName = "Petrenko O. S.";
            test1.DateOfPassing = new DateTime(2012, 12, 12);
            test1.GroupName = "ITP 12-51";
            test1.StudentName = "Fedotov Olexandr Ivanovich";
            test1.IsPassed = true;
            test1.TestTitle = "Simple OOP test #1";

            TestInfo test2 = new TestInfo();
            test2.AuthorName = "Olexandrov V. V.";
            test2.DateOfPassing = new DateTime(2013, 10, 2);
            test2.GroupName = "IUS 08-1";
            test2.StudentName = "Kirilenko Oleg Victorovich";
            test2.IsPassed = false;
            test2.TestTitle = "Knowledge of basic axioms";

            dm1.TestInfo = new List<TestInfo>();
            dm1.TestInfo.Add(test1);
            dm1.TestInfo.Add(test2);

            UserStatisticDataModel dm2 = new UserStatisticDataModel();
            dm2.Subject = "Programming";

            dm2.TestInfo = new List<TestInfo>();
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test2);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test2);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test2);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test2);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test2);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test1);
            dm2.TestInfo.Add(test2);

            
            Statistics.Add(dm1);
            Statistics.Add(dm2);
            
            SelectedStatistic = dm1;
            #endregion
        }
    }
}
