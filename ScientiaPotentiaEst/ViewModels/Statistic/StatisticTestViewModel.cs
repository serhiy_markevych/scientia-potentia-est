﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelsLibrary;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.ViewModels.Statistic
{
    /// <summary>
    /// ViewModel for statistic of test
    /// </summary>
    /// <author>Cherednychok Ivan</author>
    /// <date>13 May 2013</date>
    public class StatisticTestViewModel
    {
        public StatisticTestViewModel() {
            TestStatistic = new TestStatisticDataModel();

            // fake data //TODO: Replace by method from StatisticService
            TestStatistic.Title = "Basic knowledge of OOP";
            TestStatistic.Author = "Grigorovich O. P.";
            TestStatistic.DateOfPassing = new DateTime(2013, 4, 22);
            TestStatistic.IsPassed = true;
            TestStatistic.Subject = "Programming";
            TestStatistic.TotalQuestions = 3;
            TestStatistic.TotalCorrectAnswers = 2;
            TestStatistic.UserFullName = "Ivanov Olexandr Olegovich";
            TestStatistic.UserGroup = "MT 12-41";

            List<QuestionInfo> questions = new List<QuestionInfo>();
            questions.Add(new QuestionInfo() { OrdinalNumber = 1, IsCorrect = true, Title = "What is OOP?" });
            questions.Add(new QuestionInfo() { OrdinalNumber = 2, IsCorrect = false, Title = "Explain what is polimorphizm" });
            questions.Add(new QuestionInfo() { OrdinalNumber = 3, IsCorrect = true, Title = "Difference between types by value and types by reference" });

            TestStatistic.QuestionsInfo = questions;
        }

        public string TestResult
        {
            get
            {
                if (this.TestStatistic.IsPassed)
                {
                    return DisplayLabels.TestPassedYes;
                }
                else
                {
                    return DisplayLabels.TestPassedNo;
                }
                
            }
        }
        public TestStatisticDataModel TestStatistic { get; set; }
    }
}
