﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScientiaPotentiaEst.ViewModels
{
    /// <summary>
    /// ViewModel for user main window
    /// </summary>
    /// <author>Evgeny Drapoguz</author>
    /// <date>13 May 2013</date>
    class MainViewModel
    {
        #region Fields
        private string _userName;
        private string _userGroup;
        #endregion


        #region Property
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string UserGroup
        {
            get { return _userGroup; }
            set { _userGroup = value; }
        }
        #endregion


        #region Constructors
        public MainViewModel()
        {
            _userName = "";
            _userGroup = "";
        }

        public MainViewModel(string userName, string userGroup)
        {
            _userName = userName;
            _userGroup = userGroup;
        }
        #endregion
    }
}
