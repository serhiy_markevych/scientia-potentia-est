﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using ScientiaPotentiaEst;

/// <summary>
/// Calculate colors for button 
/// </summary>
/// <author>Alina Telyk</author>
/// <date>13 May 2013</date>
namespace ScientiaPotentiaEst.Controls
{
    public partial class EventClass
    {

        private void ButtonColor(object sender, MouseEventArgs e)
        {
            Color basic = ((SolidColorBrush)((Button)sender).Background).Color;
            Application.Current.Resources["LightButtonBrush"] = Lighten(basic);
            Application.Current.Resources["DarkButtonBrush"] = Darken(basic);
        }

        private SolidColorBrush Lighten(Color basic)
        {
            Color lighten = Color.FromArgb((byte)255, (byte)(basic.R + ((255 - basic.R) / 2.5)),
                (byte)(basic.G + ((255 - basic.G) / 2.5)),
                (byte)(basic.B + ((255 - basic.B) / 2.5)));
            return new SolidColorBrush(lighten);
        }

        private SolidColorBrush Darken(Color basic)
        {
            Color darken = Color.FromArgb((byte)255, (byte)(basic.R / 1.7),
                 (byte)(basic.G / 1.7),
                 (byte)(basic.B / 1.7));
            return new SolidColorBrush(darken);
        }
    }
}
