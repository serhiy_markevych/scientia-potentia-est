﻿<!--
///<summary>
/// ComboBox
/// Derive the hint message by dint of Tag on Validation.HasError 
/// Total height with hint = ActualHeight + 22px
///</summary>
///
<author>Alina Telyk</author>
///
<date>13 May 2013</date>
-->
<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
                    mc:Ignorable="d">
    <ResourceDictionary.MergedDictionaries>
        <ResourceDictionary Source="MainShare.xaml"/>
    </ResourceDictionary.MergedDictionaries>
    
    <Style x:Key="ComboBoxFocusVisual">
        <Setter Property="Control.Template">
            <Setter.Value>
                <ControlTemplate>
                    <Rectangle SnapsToDevicePixels="true" Stroke="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}" StrokeThickness="1" StrokeDashArray="1 2"/>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    
    
    <Style TargetType="{x:Type ToggleButton}">
        <Setter Property="OverridesDefaultStyle" Value="true"/>
        <Setter Property="IsTabStop" Value="false"/>
        <Setter Property="Background" Value="{StaticResource WhiteBrush}"/>
        <Setter Property="Focusable" Value="false"/>
        <Setter Property="Margin" Value="1"/>
        <Setter Property="ClickMode" Value="Press"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type ToggleButton}">
                    <Grid>
                        <Rectangle Fill="{TemplateBinding Background}" Stroke="{TemplateBinding BorderBrush}" />
                        <Grid HorizontalAlignment="Right" Width="15">
                            <Path x:Name="Arrow" Data="{StaticResource DownArrowGeometry}" Fill="{DynamicResource MainBlueBrush}" HorizontalAlignment="Center"  VerticalAlignment="Center"/>
                        </Grid>
                    </Grid>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsChecked" Value="true">
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="false">
                            <Setter Property="Fill" TargetName="Arrow" Value="{StaticResource LightGreyBrush}"/>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="ComboBoxEditableTextBox" TargetType="{x:Type TextBox}">
        <Setter Property="AllowDrop" Value="true"/>
        <Setter Property="MinWidth" Value="0"/>
        <Setter Property="MinHeight" Value="23"/>
        <Setter Property="Foreground" Value="{StaticResource BlackBrush}"/>
        <Setter Property="FocusVisualStyle" Value="{x:Null}"/>
        <Setter Property="ScrollViewer.PanningMode" Value="VerticalFirst"/>
        <Setter Property="Stylus.IsFlicksEnabled" Value="False"/>
        <Setter Property="Margin" Value="5"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type TextBox}">
                    <ScrollViewer x:Name="PART_ContentHost"  Focusable="false" HorizontalScrollBarVisibility="Hidden" VerticalScrollBarVisibility="Hidden"/>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
    
    <ControlTemplate x:Key="ComboBoxEditableTemplate" TargetType="{x:Type ComboBox}">
        <Grid x:Name="Placement" SnapsToDevicePixels="true">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition MinWidth="{DynamicResource {x:Static SystemParameters.VerticalScrollBarWidthKey}}" Width="0"/>
            </Grid.ColumnDefinitions>
            <Popup x:Name="PART_Popup" AllowsTransparency="true" Grid.ColumnSpan="2" IsOpen="{Binding IsDropDownOpen, RelativeSource={RelativeSource TemplatedParent}}" Margin="1,11,1,1" Placement="Bottom" PopupAnimation="Fade">
                <Grid x:Name="Shdw" Background="Transparent" MaxHeight="{TemplateBinding MaxDropDownHeight}" MinWidth="{Binding ActualWidth, ElementName=Placement}" Margin="0">
                    <Border x:Name="DropDownBorder" BorderBrush="{DynamicResource MiddleGreyBrush}" BorderThickness="1" Background="{StaticResource WhiteBrush}" Margin="0,5,0,0">
                        <ScrollViewer x:Name="DropDownScrollViewer" Margin="0,3" Template="{DynamicResource MetroScrollViewerControlTemplate}">
                            <Grid RenderOptions.ClearTypeHint="Enabled">
                                <Canvas HorizontalAlignment="Left" Height="0" VerticalAlignment="Top" Width="0">
                                    <Rectangle x:Name="OpaqueRect" Fill="{Binding Background, ElementName=DropDownBorder}" Height="{Binding ActualHeight, ElementName=DropDownBorder}" Width="{Binding ActualWidth, ElementName=DropDownBorder}"/>
                                </Canvas>
                                <ItemsPresenter x:Name="ItemsPresenter" KeyboardNavigation.DirectionalNavigation="Contained" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                            </Grid>
                        </ScrollViewer>
                    </Border>
                    <Path x:Name="Arrow" Data="{StaticResource UpArrowGeometry}" Fill="{DynamicResource WhiteBrush}" HorizontalAlignment="Left" Margin="25,0,0,0" VerticalAlignment="Top" Height="6" Stretch="Fill" Width="10" RenderTransformOrigin="0.5,0.5" Stroke="{DynamicResource MiddleGreyBrush}"/>
                    
                    <Path Data="M0,0 L10,0" Fill="{DynamicResource WhiteBrush}" HorizontalAlignment="Left" Height="1" Margin="25.5,5,0,0" Stretch="Fill" Stroke="{DynamicResource WhiteBrush}" VerticalAlignment="Top" Width="9"/>

                </Grid>
            </Popup>
            <ToggleButton BorderBrush="{TemplateBinding BorderBrush}" Background="{TemplateBinding Background}" Grid.ColumnSpan="2" IsChecked="{Binding IsDropDownOpen, Mode=TwoWay, RelativeSource={RelativeSource TemplatedParent}}" Foreground="{DynamicResource MainBlueBrush}"/>
            <TextBox x:Name="PART_EditableTextBox" Foreground="{TemplateBinding Foreground}" HorizontalContentAlignment="{TemplateBinding HorizontalContentAlignment}" IsReadOnly="{Binding IsReadOnly, RelativeSource={RelativeSource TemplatedParent}}" Margin="{TemplateBinding Padding}" Style="{StaticResource ComboBoxEditableTextBox}" VerticalContentAlignment="{TemplateBinding VerticalContentAlignment}"/>
            <Grid x:Name="Validate_hint" Height="15" MinWidth="37" MaxWidth="{Binding ActualWidth, ElementName=Box}" Margin="0,0,0,-21" VerticalAlignment="Bottom" HorizontalAlignment="Left" Visibility="Hidden">
                <Rectangle Fill="{DynamicResource WhiteBrush}" Height="Auto" Width="Auto" Stroke="{DynamicResource MiddleGreyBrush}"/>
                <ContentPresenter  TextBlock.FontSize="9" Content="{TemplateBinding Tag}" Margin="2,1"/>
                <Path Data="{StaticResource UpArrowGeometry}" Fill="{DynamicResource WhiteBrush}" HorizontalAlignment="Left" Margin="25,-5,0,0" VerticalAlignment="Top" Height="6" Stretch="Fill" Width="10" RenderTransformOrigin="0.5,0.5" Stroke="{DynamicResource MiddleGreyBrush}"/>
                <Path Data="M0,5 L10,5" Fill="{DynamicResource WhiteBrush}" HorizontalAlignment="Left" Height="1" Margin="25.5,0,0,0" Stretch="Fill" Stroke="{DynamicResource WhiteBrush}" VerticalAlignment="Top" Width="9"/>
            </Grid>
        </Grid>
        <ControlTemplate.Triggers>
           <Trigger Property="IsMouseOver" Value="true">
                <Setter Property="BorderBrush" Value="{StaticResource LightBlueBrush}"/>
            </Trigger>
            <Trigger Property="IsEnabled" Value="false">
                <Setter Property="Foreground" Value="{StaticResource LightGreyBrush}"/>
                <Setter Property="Background" Value="{StaticResource WhiteBrush}"/>
                <Setter Property="BorderBrush" Value="{StaticResource LightGreyBrush}"/>
            </Trigger>
            <Trigger Property="IsKeyboardFocusWithin" Value="true">
                <Setter Property="Foreground" Value="{StaticResource BlackBrush}"/>
            </Trigger>
            <Trigger Property="HasItems" Value="false">
                <Setter Property="Height" TargetName="DropDownBorder" Value="95"/>
            </Trigger>
            <Trigger Property="IsGrouping" Value="true">
                <Setter Property="ScrollViewer.CanContentScroll" Value="false"/>
            </Trigger>
            <Trigger Property="ScrollViewer.CanContentScroll" SourceName="DropDownScrollViewer" Value="false">
                <Setter Property="Canvas.Top" TargetName="OpaqueRect" Value="{Binding VerticalOffset, ElementName=DropDownScrollViewer}"/>
                <Setter Property="Canvas.Left" TargetName="OpaqueRect" Value="{Binding HorizontalOffset, ElementName=DropDownScrollViewer}"/>
            </Trigger>
            <Trigger Property="Validation.HasError" Value="true">
                <Setter Property="Tag" Value="{Binding RelativeSource={x:Static RelativeSource.Self}, Path=(Validation.Errors).CurrentItem.ErrorContent}" />
                <Setter Property="Visibility" TargetName="Validate_hint" Value="Visible"/>
            </Trigger>
        </ControlTemplate.Triggers>
    </ControlTemplate>
    
    <Style TargetType="{x:Type ComboBox}" BasedOn="{StaticResource MainFontStyle}">
        <Setter Property="FocusVisualStyle" Value="{StaticResource ComboBoxFocusVisual}"/>
        <Setter Property="Foreground" Value="{DynamicResource BlackBrush}"/>
        <Setter Property="Background" Value="{DynamicResource WhiteBrush}"/>
        <Setter Property="BorderBrush" Value="{DynamicResource MiddleGreyBrush}"/>
        <Setter Property="BorderThickness" Value="1"/>
        <Setter Property="Padding" Value="4,3,2,2"/>
        <Setter Property="ScrollViewer.CanContentScroll" Value="true"/>
        <Setter Property="ScrollViewer.PanningMode" Value="Both"/>
        <Setter Property="Stylus.IsFlicksEnabled" Value="False"/>
        <Setter Property="VerticalContentAlignment" Value="Top"/>
        <Setter Property="SelectedIndex" Value="0"/>
        <Setter Property="MinHeight" Value="23"/>
        <Setter Property="Margin" Value="5"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type ComboBox}">
                    <Grid x:Name="MainGrid" SnapsToDevicePixels="true">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*"/>
                            <ColumnDefinition MinWidth="{DynamicResource {x:Static SystemParameters.VerticalScrollBarWidthKey}}" Width="0"/>
                        </Grid.ColumnDefinitions>
                        <Popup x:Name="PART_Popup" AllowsTransparency="true" Grid.ColumnSpan="2" IsOpen="{Binding IsDropDownOpen, RelativeSource={RelativeSource TemplatedParent}}" Margin="1,11,1,1" Placement="Bottom" PopupAnimation="Fade">
                            <Grid x:Name="Shdw" Background="Transparent" MaxHeight="{TemplateBinding MaxDropDownHeight}" MinWidth="{Binding ActualWidth, ElementName=MainGrid}" Margin="0">
                                <Border x:Name="DropDownBorder" BorderBrush="{DynamicResource MiddleGreyBrush}" BorderThickness="1" Background="{StaticResource WhiteBrush}" Margin="0,5,0,0">
                                    <ScrollViewer x:Name="DropDownScrollViewer" Margin="0,3" Template="{DynamicResource MetroScrollViewerControlTemplate}">
                                        <Grid RenderOptions.ClearTypeHint="Enabled">
                                            <Canvas HorizontalAlignment="Left" Height="0" VerticalAlignment="Top" Width="0">
                                                <Rectangle x:Name="OpaqueRect" Fill="{Binding Background, ElementName=DropDownBorder}" Height="{Binding ActualHeight, ElementName=DropDownBorder}" Width="{Binding ActualWidth, ElementName=DropDownBorder}"/>
                                            </Canvas>
                                            <ItemsPresenter x:Name="ItemsPresenter" KeyboardNavigation.DirectionalNavigation="Contained" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                                        </Grid>
                                    </ScrollViewer>

                                </Border>

                                <Path x:Name="Arrow" Data="{StaticResource UpArrowGeometry}" Fill="{DynamicResource WhiteBrush}" HorizontalAlignment="Left" Margin="25,0,0,0" VerticalAlignment="Top" Height="6" Stretch="Fill" Width="10" RenderTransformOrigin="0.5,0.5" Stroke="{DynamicResource MiddleGreyBrush}"/>
                                
                                <Path Data="M0,5 L10,5" Fill="{DynamicResource WhiteBrush}" HorizontalAlignment="Left" Height="1" Margin="25.5,5,0,0" Stretch="Fill" Stroke="{DynamicResource WhiteBrush}" VerticalAlignment="Top" Width="9"/>

                            </Grid>
                        </Popup>
                        <ToggleButton BorderBrush="{TemplateBinding BorderBrush}" Background="{TemplateBinding Background}" Grid.ColumnSpan="2" IsChecked="{Binding IsDropDownOpen, Mode=TwoWay, RelativeSource={RelativeSource TemplatedParent}}" Foreground="{DynamicResource MainBlueBrush}"/>
                        <ContentPresenter ContentTemplate="{TemplateBinding SelectionBoxItemTemplate}" ContentTemplateSelector="{TemplateBinding ItemTemplateSelector}" Content="{TemplateBinding SelectionBoxItem}" ContentStringFormat="{TemplateBinding SelectionBoxItemStringFormat}" HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" IsHitTestVisible="false" Margin="{TemplateBinding Padding}" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}" VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                    </Grid>
                    <ControlTemplate.Triggers>
                        <Trigger Property="HasItems" Value="false">
                            <Setter Property="Height" TargetName="DropDownBorder" Value="95"/>
                        </Trigger>
                        <Trigger Property="IsMouseOver" Value="true">
                            <Setter Property="BorderBrush" Value="{StaticResource LightBlueBrush}"/>
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="false">
                            <Setter Property="Foreground" Value="{StaticResource LightGreyBrush}"/>
                            <Setter Property="Background" Value="{StaticResource WhiteBrush}"/>
                            <Setter Property="BorderBrush" Value="{StaticResource LightGreyBrush}"/>
                        </Trigger>
                        <Trigger Property="IsGrouping" Value="true">
                            <Setter Property="ScrollViewer.CanContentScroll" Value="false"/>
                        </Trigger>
                        <Trigger Property="ScrollViewer.CanContentScroll" SourceName="DropDownScrollViewer" Value="false">
                            <Setter Property="Canvas.Top" TargetName="OpaqueRect" Value="{Binding VerticalOffset, ElementName=DropDownScrollViewer}"/>
                            <Setter Property="Canvas.Left" TargetName="OpaqueRect" Value="{Binding HorizontalOffset, ElementName=DropDownScrollViewer}"/>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Style.Triggers>
            <Trigger Property="IsEditable" Value="true">
                <Setter Property="IsTabStop" Value="false"/>
                <Setter Property="Padding" Value="1,2,2,2"/>
                <Setter Property="Template" Value="{StaticResource ComboBoxEditableTemplate}"/>
            </Trigger>
        </Style.Triggers>
    </Style>

    <Style TargetType="{x:Type ComboBoxItem}" d:IsControlPart="True">
        <Setter Property="SnapsToDevicePixels" Value="true"/>
        <Setter Property="Height" Value="20"/>
        <Setter Property="VerticalContentAlignment" Value="Center"/>
        <Setter Property="HorizontalContentAlignment" Value="Stretch"/>
        <Setter Property="FontSize" Value="12"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type ComboBoxItem}">
                    <Grid SnapsToDevicePixels="true">
                        <Border x:Name="Border" Background="{TemplateBinding Background}" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}"/>
                        <ContentPresenter x:Name="contentPresenter" HorizontalAlignment="Stretch" VerticalAlignment="{TemplateBinding VerticalContentAlignment}" Margin="4,0,0,0"/>
                    </Grid>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsHighlighted" Value="true">
                            <Setter Property="Background" TargetName="Border" Value="{StaticResource MainBlueBrush}"/>
                            <Setter Property="Foreground" Value="{StaticResource WhiteBrush}"/>
                        </Trigger>
                        <Trigger Property="IsEnabled" Value="false">
                            <Setter Property="Foreground" Value="{StaticResource LightGreyBrush}"/>
                        </Trigger>
                        <MultiTrigger>
                            <MultiTrigger.Conditions>
                                <Condition Property="IsEnabled" Value="false"/>
                                <Condition Property="IsSelected" Value="true"/>
                            </MultiTrigger.Conditions>
                            <Setter Property="Background" TargetName="Border" Value="{StaticResource LightGreyBrush}"/>
                            <Setter Property="TextBlock.Foreground" TargetName="contentPresenter" Value="{StaticResource WhiteBrush}"/>
                        </MultiTrigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
</ResourceDictionary>