﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScientiaPotentiaEst.Windows.User;
using ScientiaPotentiaEst.Windows.Subject;
using ScientiaPotentiaEst.ViewModels;
using CommonLibrary;
using ResourcesLibrary;
using ScientiaPotentiaEst.Windows.Testing;
using ScientiaPotentiaEst.Windows.Statistic;
using DataModelsLibrary;

namespace ScientiaPotentiaEst
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields
        private MainViewModel _viewModel;
        #endregion


        #region Property
        private MainViewModel ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }
        #endregion


        #region Methods
        private void InitViewModel()
        {
            _viewModel = new MainViewModel()
            {
                UserName = Authentication.CurrentUser.FullName,
                UserGroup = (Authentication.CurrentUser.Group != null) ? Authentication.CurrentUser.Group.Name : null
            };
        }

        private MainViewModel GetViewModel()
        {
            if (_viewModel == null)
            {
                InitViewModel();
            }

            return _viewModel;
        }
        #endregion


        #region Constructors
        public MainWindow()
        {
            InitializeComponent();

            UserAuthenticationWindow userAuthWindow = new UserAuthenticationWindow();
            userAuthWindow.ShowDialog();

            if (Authentication.CurrentUser != null)
            {
                DataContext = GetViewModel();

                // show statistic button only for Students or Teachers
                if (Authentication.CurrentUser.HasRole(UserRole.Student) || Authentication.CurrentUser.HasRole(UserRole.Teacher))
                {
                    btnMainUserStatistic.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }
        #endregion


        #region Constrols Methods
        private void btnMainUserProfile_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(Messages.FunctionNotImplemented);
        }

        private void btnMainUserStatistic_Click(object sender, RoutedEventArgs e)
        {
            if (Authentication.CurrentUser.HasRole(UserRole.Student))
            {
                StatisticStudentWindow userStatisticWindow = new StatisticStudentWindow();
                userStatisticWindow.ShowDialog();
            }
            else if (Authentication.CurrentUser.HasRole(UserRole.Teacher))
            {
                StatisticTeacherWindow userStatisticWindow = new StatisticTeacherWindow();
                userStatisticWindow.ShowDialog();
            }
            
        }

        private void btnMainUSerSignOut_Click(object sender, RoutedEventArgs e)
        {
            Authentication.Logout();

            this.Hide();

            UserAuthenticationWindow userAuthWindow = new UserAuthenticationWindow();
            userAuthWindow.ShowDialog();

            if (Authentication.CurrentUser != null)
            {
                this.InitViewModel();
                DataContext = GetViewModel();

                this.Show();
            }
        }

        private void btnMainShowTestingWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();

            TestingWindow testingwindow = new TestingWindow();
            testingwindow.ShowDialog();

            this.Show();
        }
        #endregion
    }
}
