﻿using ScientiaPotentiaEst.ViewModels.Assign;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScientiaPotentiaEst.Windows.Assign
{
    /// Logic for AssignWindow.xaml
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>17.05.2013</date>
    public partial class AssignWindow : Window
    {
        private AssignCollectionViewModel _assignsCollection;

        public AssignWindow()
        {
            _assignsCollection = new AssignCollectionViewModel();
            this.DataContext = _assignsCollection;
            InitializeComponent();
        }
    }
}
