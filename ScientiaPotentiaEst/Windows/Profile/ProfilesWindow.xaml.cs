﻿using ScientiaPotentiaEst.ViewModels.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScientiaPotentiaEst.Windows.Profile
{
    /// Logic for ProfilesWindow.xaml
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>15.05.2013</date>
    public partial class ProfilesWindow : Window
    {
        private ProfilesCollectionViewModel _profilesCollection;

        public ProfilesWindow()
        {
            _profilesCollection = new ProfilesCollectionViewModel();
            this.DataContext = _profilesCollection;
            InitializeComponent();
        }
    }
}
