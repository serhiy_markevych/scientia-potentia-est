﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Modul;

namespace ScientiaPotentiaEst.Windows.Modul
{
    /// <summary>
    /// Interaction logic for ModulActionWindow.xaml
    /// It's just for test! It will be goodly recreated by O. Lysenko!
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public partial class ModulActionWindow : Window
    {
        public ModulActionWindow()
        {
            InitializeComponent();
        }

        public ModulActionWindow(ModulViewModel modulViewModel)
        {
            InitializeComponent();
            if (modulViewModel == null)
                return;
            modulViewModel.SetCurrentWindow(this);
            DataContext = modulViewModel;
        }
    }
}
