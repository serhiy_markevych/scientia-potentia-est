﻿using System;
using System.Windows;
using System.Windows.Controls;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Testing;
using ScientiaPotentiaEst.Windows.Testing.UIQuestionHandler;
using ResourcesLibrary;
using System.Windows.Threading;
using ScientiaPotentiaEst.Windows.SettingTest;

namespace ScientiaPotentiaEst.Windows.Testing
{
    /// <summary>
    /// Main window of the subsystems of the test
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    /// <refactor>Dmitriy Zhemkov</refactor>
    /// <date>28.05.2013</date>
    public partial class TestingWindow : Window
    {
        #region fields

        private TimeSpan _dtime = new TimeSpan(0, 0, 1);
        private TimeSpan _timeofend ;
        private DispatcherTimer _timer;

        #endregion

        #region Property
        public TestingViewModel Curent;
        public StubTesting Testing;   
        #endregion

        #region Constructors
        public TestingWindow()
        {
            InitializeComponent();
            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(dispatcherTimer_Tick);              
        }
        #endregion

        #region Method control event
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //StateOfTesting(false); 
            SettingTestWindow wnSeting = new SettingTestWindow();
            if (wnSeting.ShowDialog() == true)
            {
                Testing = new StubTesting(wnSeting.Data.Mode, wnSeting.Data.SelectedModul);
                btnCorrect.Visibility = wnSeting.Data.IsLearningMode ? Visibility.Visible : Visibility.Hidden;
                StateOfTesting(true);
                btnNextQuestion_Click(this, null);
            }
            else this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StateOfTesting(false);
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Window_Loaded(this, null);       
        }
           
        private void btnFinishTest_Click(object sender, RoutedEventArgs e)
        {           
            Testing.Complit();
            Close();
        }

        private void btnNextQuestion_Click(object sender, RoutedEventArgs e)
        {
            Curent = null;
            Curent = Testing.NextQuestion();
            VisibleQuestionHandlerUI();
            stpQuestion.IsEnabled = true;
        }

        private void btnPrevQuestion_Click(object sender, RoutedEventArgs e)
        {
            Curent = null;
            Curent = Testing.PrevQuestion();
            VisibleQuestionHandlerUI();
            stpQuestion.IsEnabled = true;
        }

        private void btnCorrect_Click(object sender, RoutedEventArgs e)
        {
            if (stpQuestion.Children.Count == 1)
            {
                TestingViewModel temp = Testing.CorrectCurentAnswer();
                if (temp != null)
                {
                    UserControl ui = CreateUI(temp);
                    if (ui != null)
                    {
                        stpQuestion.Children.Add(ui);
                        ui.Width = stpQuestion.Width;
                        ui.Height = stpQuestion.Height;
                    }
                    stpQuestion.IsEnabled = false;
                }
                else MessageBox.Show(Messages.QuestionNotFound, Messages.Warning);
            }
        }
        #endregion

        #region private method
        private void VisibleQuestionHandlerUI()
        {
            if (Curent != null)
            {
                UserControl ui = CreateUI(Curent);
                if (ui != null)
                {
                    stpQuestion.Children.Clear();
                    stpQuestion.Children.Add(ui);
                    ui.Width = stpQuestion.Width;
                    ui.Height = stpQuestion.Height;
                }
            }
            else MessageBox.Show(Messages.QuestionNotFound, Messages.Warning);
        }

        private UserControl CreateUI(TestingViewModel curent)
        {
            UserControl ui = null;
            switch (Curent.Question.Type)
            {
                case QuestionType.OneAnswer:
                    ui = new OneAnswerUI(curent);
                    break;
                case QuestionType.MultipleAnswers:
                    ui = new ManyAnswerUI(curent);
                    break;
                case QuestionType.TypeAnswer:
                    ui = new TypeAnswerUI(curent);
                    break;
                case QuestionType.Compliance:
                    ui = new ComplianceUI(curent);
                    break;
                case QuestionType.Ordering:
                    ui = new OrderingUI(curent);
                    break;
                case QuestionType.Classification:
                    ui = new ClassificationUI(curent);
                    break;
                default:
                    MessageBox.Show(Messages.InitializationNotImplementedDM, Messages.Warning);
                    break;
            }
            return ui;
        }

        private void StateOfTesting(bool on)
        {
            stpQuestion.Visibility = on ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;  
            btnBack.IsEnabled = on;

            btnCorrect.IsEnabled = on;
            btnFinishTest.IsEnabled = on;
            btnNextQuestion.IsEnabled = on;
            btnPrevQuestion.IsEnabled = on;

            if (on && Testing.Mode == TestMode.Testing)
            {
                _timeofend = Testing.Time;
                _timer.Interval = _dtime;
                lbTime.Content = _timeofend.ToString(@"hh\:mm\:ss");
                _timer.Start();
            }
            else _timer.Stop();
            
            if(Testing != null && Testing.Mode==TestMode.Testing)
                stpTime.Visibility = on ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _timeofend -= _dtime;
            lbTime.Content = _timeofend.ToString(@"hh\:mm\:ss");
            if (_timeofend.TotalSeconds < 0)
            {
                _timer.Stop();
                MessageBox.Show(Messages.TimeEnd, Messages.TestEnd);
                btnFinishTest_Click(null, null);
            }
        }
        #endregion
    }
}
