﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Testing;
using ScientiaPotentiaEst.ViewModels.Testing.UIQuestionViewModel;
using DataModelsLibrary;


namespace ScientiaPotentiaEst.Windows.Testing.UIQuestionHandler
{
    /// <summary>
    /// class UserControl test question for classification
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public partial class ClassificationUI : UserControl
    {
        public ClassificationHandlerViewModel _curent;

        public ClassificationUI(TestingViewModel curent)
        {
            InitializeComponent();
            _curent = new ClassificationHandlerViewModel(curent);            
            DataContext = _curent;
            
        }
        
        private void btnGroping_Click(object sender, RoutedEventArgs e)
        {
            AnswerDataModel curentanswer = lsbanswer.SelectedItem as AnswerDataModel;
            string curentgrup = lsbgroup.SelectedItem as string;
            if ((curentanswer != null) && (curentgrup != null))
            {
                _curent.Grouping(curentanswer, curentgrup);
                lsbanswer.Items.Refresh();
                lsbgroupinganswer.Items.Refresh();
                lsbanswer.SelectedIndex = 0;
                lsbgroupinganswer.SelectedIndex = 0;
            }
           
        }

        private void btnDeGroping_Click(object sender, RoutedEventArgs e)
        {
            AnswerDataModel curentanswer = lsbgroupinganswer.SelectedItem as AnswerDataModel;
            if (curentanswer != null)
            {
                _curent.DeGrouping(curentanswer);
                lsbanswer.Items.Refresh();
                lsbgroupinganswer.Items.Refresh();
                lsbanswer.SelectedIndex = 0;
                lsbgroupinganswer.SelectedIndex = 0;
            }
           
        }
    }
}
