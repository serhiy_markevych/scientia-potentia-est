﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Testing;
using DataModelsLibrary;

namespace ScientiaPotentiaEst.Windows.Testing.UIQuestionHandler
{
    /// <summary>
    /// class UserControl test question for ManyAnswer
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public partial class ManyAnswerUI : UserControl
    {
        public TestingViewModel _curent;

        public ManyAnswerUI(TestingViewModel curent)
        {
            InitializeComponent();
            _curent = curent;
            DataContext = _curent;
            if (_curent.TestAnswer == null)
            {
                _curent.TestAnswer = new List<TestAnswerDataModel>();
                int countcorect = _curent.Answers.Count(w => w.Significance == 1);
                for (int i = 0; i < countcorect; i++)
                    _curent.TestAnswer.Add(new TestAnswerDataModel());
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox obj = sender as CheckBox;
            int id;
            if (obj != null)
                if (obj.Tag != null)
                {
                    id = (int)obj.Tag;
                    AnswerDataModel tempanswer = _curent.Answers.FirstOrDefault(w => w.ID == id);
                    if (tempanswer != null)
                    {
                        TestAnswerDataModel testanswer = null;
                        if (tempanswer.IsChecked == true)
                        {
                            testanswer = _curent.TestAnswer.FirstOrDefault(w => (w.AnswerID == 0) || (w.AnswerID == null));
                            if (testanswer == null)
                                tempanswer.IsChecked = false;
                            else
                                testanswer.AnswerID = id;
                        }
                        else
                        {
                            testanswer = _curent.TestAnswer.FirstOrDefault(w => w.AnswerID == id);
                            testanswer.AnswerID = 0;
                        }
                    }
                }
            lsbx_answer.Items.Refresh();
        }
    }
}
