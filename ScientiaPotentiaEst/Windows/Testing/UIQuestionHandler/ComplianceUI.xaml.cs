﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Testing;
using ScientiaPotentiaEst.ViewModels.Testing.UIQuestionViewModel;
using DataModelsLibrary;

namespace ScientiaPotentiaEst.Windows.Testing.UIQuestionHandler
{
    /// <summary>
    /// class UserControl test question for compliance
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public partial class ComplianceUI : UserControl
    {
        public ComplianceHandlerViewModel _curent;

        public ComplianceUI(TestingViewModel curent)
        {
            InitializeComponent();
            _curent = new ComplianceHandlerViewModel(curent);
            lsb_answerdinamic.SelectedIndex = 0;
            DataContext = _curent;
        }

        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            AnswerDataModel obj = lsb_answerdinamic.SelectedItem as AnswerDataModel;
            if (obj != null)          
                _curent.Up(obj);
            lsb_answerdinamic.Items.Refresh();
        }

        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            AnswerDataModel obj = lsb_answerdinamic.SelectedItem as AnswerDataModel;
            if (obj != null)
                _curent.Down(obj);
            lsb_answerdinamic.Items.Refresh();
        }
               
    }
}
