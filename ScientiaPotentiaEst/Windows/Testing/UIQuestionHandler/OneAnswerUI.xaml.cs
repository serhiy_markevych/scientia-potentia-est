﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Testing;
using DataModelsLibrary;

namespace ScientiaPotentiaEst.Windows.Testing.UIQuestionHandler
{
    /// <summary>
    /// class UserControl test question for OneAnswer
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>11 May 2013</date>
    public partial class OneAnswerUI : UserControl
    {
        public TestingViewModel _curent;
        public OneAnswerUI(TestingViewModel curent)
        {
            InitializeComponent();            
            _curent = curent;            
            DataContext = curent;
            if (_curent.TestAnswer == null)
            {
                _curent.TestAnswer = new List<TestAnswerDataModel>();
                _curent.TestAnswer.Add(new TestAnswerDataModel());
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {            
            RadioButton obj = sender as RadioButton;
            if (obj != null && obj.Tag != null)
            {
                _curent.TestAnswer[0].AnswerID = (int)obj.Tag;
            }
        }
    }
}
