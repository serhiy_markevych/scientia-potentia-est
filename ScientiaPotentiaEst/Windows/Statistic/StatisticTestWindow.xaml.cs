﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ResourcesLibrary;
using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Statistic;

namespace ScientiaPotentiaEst.Windows.Statistic
{
    /// <summary>
    ///    Interaction logic for StatisticStudentWindow.xaml
    /// </summary>
    /// <author>Sergey Frynzko</author>
    /// <date>12.05.2013</date>
    public partial class StatisticTestWindow : Window
    {
        private StatisticTestViewModel _viewModel;
        private void InitViewModel() {
            _viewModel = new StatisticTestViewModel();
            this.DataContext = _viewModel;
        }

        public StatisticTestWindow()
        {
            InitializeComponent();
            InitViewModel();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
