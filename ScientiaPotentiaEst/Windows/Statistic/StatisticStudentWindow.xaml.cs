﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Statistic;

namespace ScientiaPotentiaEst.Windows.Statistic
{
    /// <summary>
    /// Interaction logic for StatisticStudentWindow.xaml
    /// </summary>
    public partial class StatisticStudentWindow : Window
    {
        private StatisticStudentViewModel _viewModel;
        public StatisticStudentWindow()
        {
            InitializeComponent();
            _viewModel = new StatisticStudentViewModel();
            DataContext = _viewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnTestDetail_Click(object sender, RoutedEventArgs e)
        {
            StatisticTestWindow wnd = new StatisticTestWindow();
            wnd.ShowDialog();
        }
    }
}
