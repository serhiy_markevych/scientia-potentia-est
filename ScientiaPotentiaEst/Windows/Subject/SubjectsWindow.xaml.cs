﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.Subject;
using ScientiaPotentiaEst.Windows.Modul;

namespace ScientiaPotentiaEst.Windows.Subject
{
    /// <summary>
    /// Interaction logic for SubjectsWindow.xaml
    /// It's just for test! It will be goodly recreated by O. Lysenko!
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>19.04.2013</date>
    public partial class SubjectsWindow : Window
    {
        private SubjectsCollectionViewModel _mainViewModel;

        public SubjectsWindow()
        {
            _mainViewModel = new SubjectsCollectionViewModel();
            this.DataContext = _mainViewModel;
            InitializeComponent();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            var wind = new ModulsWindow();
            wind.Show();
        }
    }
}
