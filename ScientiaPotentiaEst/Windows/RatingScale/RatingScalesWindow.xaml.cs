﻿using ScientiaPotentiaEst.ViewModels.Profiles;
using ScientiaPotentiaEst.ViewModels.RatingScales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScientiaPotentiaEst.Windows.RatingScale
{
    /// Logic for RatingScalesWindow.xaml
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>17.05.2013</date>
    public partial class RatingScalesWindow : Window
    {
        private RatingScalesCollectionViewModel _ratingscalesCollection;

        public RatingScalesWindow()
        {
            InitializeComponent();
        }

        public RatingScalesWindow(ProfileViewModel profile)
        {
            _ratingscalesCollection = new RatingScalesCollectionViewModel(profile);
            this.DataContext = _ratingscalesCollection;
            InitializeComponent();
        }
    }
}
