﻿using DataModelsLibrary;
using ScientiaPotentiaEst.ViewModels.Profiles;
using ScientiaPotentiaEst.ViewModels.RatingScales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScientiaPotentiaEst.Windows.RatingScale
{
    /// Logic for RatingScaleDataWindow.xaml
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>17.05.2013</date>
    public partial class RatingScaleDataWindow : Window
    {
        public RatingScaleDataWindow(RatingScaleViewModel ratingscaleVM, ProfileViewModel profile)
        {
            ratingscaleVM.Profile = new ProfilesDataModel() { ID = profile.ID, Name = profile.Name, CanceledMark = profile.CanceledMark, TimeLimit = profile.TimeLimit, ShuffleQuestions = profile.ShuffleQuestions, ShuffleAnswers = profile.ShuffleAnswers, QuestionsCount = profile.QuestionsCount, MarkType = profile.MarkType};
            InitializeComponent();
            if (ratingscaleVM == null)
                return;
            ratingscaleVM.SetCurrentWindow(this);
            DataContext = ratingscaleVM;
        }
    }
}
