﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.SubjectTesting;
using DataModelsLibrary;

namespace ScientiaPotentiaEst.Windows.SettingTest
{
    /// <summary>
    /// Seting window of the subsystems of the test
    /// </summary>
    /// <author>Shalimov Ivan</author>
    /// <date>25 May 2013</date>   
    /// <refactor>Dmitriy Zhemkov</refactor>
    /// <date>27.05.2013</date>
    public partial class SettingTestWindow : Window
    {
        public SettingTestViewModel Data;

        public SettingTestWindow()
        {
            Data = new SettingTestViewModel();
            DataContext = Data;
            InitializeComponent();            
        } 

        private void btnStartTest_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
            
    }
}
