﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.User;
using ScientiaPotentiaEst.Windows.User;
using CommonLibrary;
using ResourcesLibrary;



namespace ScientiaPotentiaEst.Windows.User
{
    /// <summary>
    /// Interaction logic for UserAuthenticationWindow.xaml
    /// </summary>
    public partial class UserAuthenticationWindow : Window
    {
        #region Fields
        private UserAuthenticationViewModel _viewModel;
        #endregion


        #region Property
        private UserAuthenticationViewModel _ViewModel
        {
            get { return GetViewModel(); }
            set { _viewModel = value; }
        }
        #endregion
        

        #region Constructors
        public UserAuthenticationWindow()
        {
            InitializeComponent();
            DataContext = GetViewModel();
        }
        #endregion


        #region Methods
        private void InitViewModel()
        {
            _viewModel = new UserAuthenticationViewModel();
        }

        private UserAuthenticationViewModel GetViewModel()
        {
            if (_viewModel == null)
            {
                InitViewModel();
            }

            return _viewModel;
        }
        #endregion


        #region Constrols Methods
        private void btnUserLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_ViewModel.IsValid())
                {
                    Authentication.Login(_ViewModel.Username, _ViewModel.Password);

                    if (Authentication.CurrentUser != null)
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(Messages.UserPasswordNotFound);
                    }
                }
                else
                {
                    this.DataContext = null;
                    this.DataContext = _ViewModel;

                    MessageBox.Show(Messages.UserPasswordNotCorrect);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnUserForgotPassword_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserResetPasswordWindow userResetPasswordWindow = new UserResetPasswordWindow();

                userResetPasswordWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                if (Authentication.CurrentUser == null)
                {
                    foreach (Window win in App.Current.Windows)
                        win.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnUserRegister_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserRegisterWindow userRegisterWindow = new UserRegisterWindow();
                userRegisterWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion  


        #region Events Methods
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnUserLogin.Focus();
                btnUserLogin.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }
        #endregion
    }
}
