﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ScientiaPotentiaEst.ViewModels.User;
using ScientiaPotentiaEst.Windows.User;
using CommonLibrary;
using CommonLibrary.Services;
using DataModelsLibrary;
using System.Threading;
using ResourcesLibrary;

namespace ScientiaPotentiaEst.Windows.User
{
    /// <summary>
    /// Interaction logic for UserResetPasswordWindow.xaml
    /// </summary>
    public partial class UserResetPasswordWindow : Window
    {
        #region Fields
        private UserResetPasswordViewModel _viewModel;
        #endregion


        #region Property
        private UserResetPasswordViewModel _ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }
        #endregion


        #region Constructors
        public UserResetPasswordWindow()
        {
            InitializeComponent();
            DataContext = GetViewModel();
        }
        #endregion


        #region Methods
        private void InitViewModel()
        {
            _viewModel = new UserResetPasswordViewModel();
        }

        private UserResetPasswordViewModel GetViewModel()
        {
            if (_viewModel == null)
            {
                InitViewModel();
            }

            return _viewModel;
        }
        #endregion


        #region Constrols Methods
        private void btnNextStep_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CommonLibrary.Validation.ValidUsername(_ViewModel.Username))
                {
                    UserDataService userDataService = new UserDataService();

                    _ViewModel.OldUserData = userDataService.GetData(u => u.Username == _ViewModel.Username).FirstOrDefault();

                    if (_ViewModel.OldUserData != null)
                    {
                        MailDataModel resetMail = new MailDataModel()
                        {
                            EmailAddress = _ViewModel.OldUserData.Email,
                            Subject = "Reset Password for " + _ViewModel.OldUserData.Username,
                            Body = "Reset Code: " + Authentication.HashPassword(_ViewModel.Username),
                        };

                        Thread mailThread = new Thread(delegate() { Mailer.SendMail(resetMail); });
                        mailThread.Start();

                        MessageBox.Show(Messages.EmailSentCode);

                        gbStep1.IsEnabled = false;

                        gbStep2.IsEnabled = true;
                    }
                    else
                    {
                        MessageBox.Show(Messages.UserNotRegistered);
                    }
                }
                else
                {
                    MessageBox.Show(Messages.UserNotCorrect);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSaveNewPassword_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_ViewModel.IsValid() && _ViewModel.SecretCode == Authentication.HashPassword(_ViewModel.Username) && CommonLibrary.Validation.ValidPassword(_ViewModel.NewPassword))
                {
                    UserDataModel newUserData = _ViewModel.OldUserData;

                    newUserData.Password = Authentication.HashPassword(_ViewModel.NewPassword);

                    UserDataService userDataService = new UserDataService();

                    userDataService.Edit(_ViewModel.OldUserData, newUserData);

                    MessageBox.Show(Messages.PasswordChanged);

                    this.Close();
                }
                else
                {
                    this.DataContext = null;
                    this.DataContext = _ViewModel;

                    MessageBox.Show(Messages.CodePasswordNotCorrect);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
