USE [master]
GO
/****** Object:  Database [ScientiaPotentiaEst]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'ScientiaPotentiaEst')
BEGIN
DECLARE @data_path nvarchar(256);
SET @data_path = (SELECT SUBSTRING(physical_name, 1, CHARINDEX(N'master.mdf', LOWER(physical_name)) - 1)
                  FROM master.sys.master_files
                  WHERE database_id = 1 AND file_id = 1);
EXECUTE ('CREATE DATABASE [ScientiaPotentiaEst] ON  PRIMARY 
( NAME = N''ScientiaPotentiaEst'', FILENAME = N''' + @data_path + 'ScientiaPotentiaEst.mdf'' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N''ScientiaPotentiaEst_log'', FILENAME = N''' + @data_path + 'ScientiaPotentiaEst_log.ldf'' , SIZE = 1024KB , MAXSIZE = 2048MB , FILEGROWTH = 10%)');
END
GO
ALTER DATABASE [ScientiaPotentiaEst] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ScientiaPotentiaEst].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ScientiaPotentiaEst] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET ARITHABORT OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ScientiaPotentiaEst] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ScientiaPotentiaEst] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ScientiaPotentiaEst] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ScientiaPotentiaEst] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ScientiaPotentiaEst] SET  READ_WRITE
GO
ALTER DATABASE [ScientiaPotentiaEst] SET RECOVERY FULL
GO
ALTER DATABASE [ScientiaPotentiaEst] SET  MULTI_USER
GO
ALTER DATABASE [ScientiaPotentiaEst] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ScientiaPotentiaEst] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'ScientiaPotentiaEst', N'ON'
GO
/****** Object:  Login [SPE-Admin]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'SPE-Admin')
CREATE LOGIN [SPE-Admin] WITH PASSWORD=N'admin', DEFAULT_DATABASE=[ScientiaPotentiaEst], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
ALTER LOGIN [SPE-Admin] ENABLE
GO
USE [ScientiaPotentiaEst]
GO
/****** Object:  User [SPE-Admin]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'SPE-Admin')
BEGIN
CREATE USER [SPE-Admin] FOR LOGIN [SPE-Admin] WITH DEFAULT_SCHEMA=[db_owner]
EXEC sp_addrolemember N'db_owner', N'SPE-Admin'
END
GO
/****** Object:  Table [dbo].[Subjects]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subjects]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Subjects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[CanceledMark] [int] NOT NULL,
 CONSTRAINT [PK_Subjects] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[CanceledMark] [int] NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Profiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Profiles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[CanceledMark] [int] NOT NULL,
	[TimeLimit] [int] NOT NULL,
	[ShuffleQuestions] [int] NOT NULL,
	[ShuffleAnswers] [int] NOT NULL,
	[QuestionsCount] [int] NOT NULL,
	[MarkType] [int] NOT NULL,
 CONSTRAINT [PK_Profiles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Groups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Groups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[CanceledMark] [int] NOT NULL,
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Users]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nchar](20) NOT NULL,
	[Password] [nvarchar](32) NOT NULL,
	[Email] [nvarchar](30) NOT NULL,
	[CanceledMark] [int] NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
	[MiddleName] [nvarchar](30) NOT NULL,
	[GroupID] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND name = N'IX_Users')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users] ON [dbo].[Users] 
(
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Moduls]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Moduls]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Moduls](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[CanceledMark] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
 CONSTRAINT [PK_Moduls] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RatingScale]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RatingScale]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RatingScale](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProfileID] [int] NOT NULL,
	[Text] [nvarchar](20) NOT NULL,
	[LowBound] [numeric](5, 2) NOT NULL,
 CONSTRAINT [PK_Rating_scale] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Questions]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Questions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Questions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrdinalNumber] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Significance] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[ModulID] [int] NOT NULL,
	[CanceledMark] [int] NOT NULL,
	[Explanation] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Questions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Tests]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tests]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tests](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ModulID] [int] NOT NULL,
	[StudentID] [int] NOT NULL,
	[TeacherID] [int] NOT NULL,
	[ProfileID] [int] NOT NULL,
	[Mode] [int] NOT NULL,
	[Date] [datetime2](7) NULL,
	[Duration] [bigint] NOT NULL,
	[Rating] [numeric](5, 2) NOT NULL,
	[TotalRating] [numeric](5, 2) NOT NULL,
	[TotalQuestions] [int] NOT NULL,
	[TotalAnswers] [int] NOT NULL,
 CONSTRAINT [PK_Tests] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UsersRoles]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UsersRoles](
	[RoleID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Users_roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Assign]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assign]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Assign](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[TeacherID] [int] NOT NULL,
	[ProfileID] [int] NOT NULL,
	[ModulID] [int] NOT NULL,
	[TestDate] [datetime2](7) NULL,
 CONSTRAINT [PK_Assign] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Answers]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Answers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Answers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionID] [int] NOT NULL,
	[OrdinalNumber] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Significance] [int] NOT NULL,
	[Group] [nvarchar](120) NOT NULL,
	[CanceledMark] [int] NOT NULL,
 CONSTRAINT [PK_Answers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TestQuestions]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TestQuestions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TestQuestions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TestID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[OrdinalNumber] [int] NOT NULL,
 CONSTRAINT [PK_Test_questions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TestAnswers]    Script Date: 04/23/2013 16:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TestAnswers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TestAnswers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TestQuestionID] [int] NOT NULL,
	[AnswerID] [int] NULL,
	[OrdinalNumber] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Test_answers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [FK_Users_Groups]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Groups]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Groups] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Groups] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Groups]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Groups]
GO
/****** Object:  ForeignKey [FK_Moduls_Subjects]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Moduls_Subjects]') AND parent_object_id = OBJECT_ID(N'[dbo].[Moduls]'))
ALTER TABLE [dbo].[Moduls]  WITH CHECK ADD  CONSTRAINT [FK_Moduls_Subjects] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subjects] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Moduls_Subjects]') AND parent_object_id = OBJECT_ID(N'[dbo].[Moduls]'))
ALTER TABLE [dbo].[Moduls] CHECK CONSTRAINT [FK_Moduls_Subjects]
GO
/****** Object:  ForeignKey [FK_RatingScale_Profiles]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RatingScale_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[RatingScale]'))
ALTER TABLE [dbo].[RatingScale]  WITH CHECK ADD  CONSTRAINT [FK_RatingScale_Profiles] FOREIGN KEY([ProfileID])
REFERENCES [dbo].[Profiles] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RatingScale_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[RatingScale]'))
ALTER TABLE [dbo].[RatingScale] CHECK CONSTRAINT [FK_RatingScale_Profiles]
GO
/****** Object:  ForeignKey [FK_Questions_Moduls]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Questions_Moduls]') AND parent_object_id = OBJECT_ID(N'[dbo].[Questions]'))
ALTER TABLE [dbo].[Questions]  WITH CHECK ADD  CONSTRAINT [FK_Questions_Moduls] FOREIGN KEY([ModulID])
REFERENCES [dbo].[Moduls] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Questions_Moduls]') AND parent_object_id = OBJECT_ID(N'[dbo].[Questions]'))
ALTER TABLE [dbo].[Questions] CHECK CONSTRAINT [FK_Questions_Moduls]
GO
/****** Object:  ForeignKey [FK_Tests_Moduls]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Moduls]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests]  WITH CHECK ADD  CONSTRAINT [FK_Tests_Moduls] FOREIGN KEY([ModulID])
REFERENCES [dbo].[Moduls] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Moduls]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests] CHECK CONSTRAINT [FK_Tests_Moduls]
GO
/****** Object:  ForeignKey [FK_Tests_Profiles]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests]  WITH CHECK ADD  CONSTRAINT [FK_Tests_Profiles] FOREIGN KEY([ProfileID])
REFERENCES [dbo].[Profiles] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests] CHECK CONSTRAINT [FK_Tests_Profiles]
GO
/****** Object:  ForeignKey [FK_Tests_Student]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Student]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests]  WITH CHECK ADD  CONSTRAINT [FK_Tests_Student] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Users] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Student]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests] CHECK CONSTRAINT [FK_Tests_Student]
GO
/****** Object:  ForeignKey [FK_Tests_Teacher]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Teacher]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests]  WITH CHECK ADD  CONSTRAINT [FK_Tests_Teacher] FOREIGN KEY([TeacherID])
REFERENCES [dbo].[Users] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tests_Teacher]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tests]'))
ALTER TABLE [dbo].[Tests] CHECK CONSTRAINT [FK_Tests_Teacher]
GO
/****** Object:  ForeignKey [FK_UsersRoles_Roles]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsersRoles_Roles]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsersRoles]'))
ALTER TABLE [dbo].[UsersRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsersRoles_Roles]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsersRoles]'))
ALTER TABLE [dbo].[UsersRoles] CHECK CONSTRAINT [FK_UsersRoles_Roles]
GO
/****** Object:  ForeignKey [FK_UsersRoles_Users]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsersRoles_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsersRoles]'))
ALTER TABLE [dbo].[UsersRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersRoles_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsersRoles_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsersRoles]'))
ALTER TABLE [dbo].[UsersRoles] CHECK CONSTRAINT [FK_UsersRoles_Users]
GO
/****** Object:  ForeignKey [FK_Assign_Groups]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Groups]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign]  WITH CHECK ADD  CONSTRAINT [FK_Assign_Groups] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Groups] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Groups]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign] CHECK CONSTRAINT [FK_Assign_Groups]
GO
/****** Object:  ForeignKey [FK_Assign_Moduls]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Moduls]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign]  WITH CHECK ADD  CONSTRAINT [FK_Assign_Moduls] FOREIGN KEY([ModulID])
REFERENCES [dbo].[Moduls] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Moduls]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign] CHECK CONSTRAINT [FK_Assign_Moduls]
GO
/****** Object:  ForeignKey [FK_Assign_Profiles]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign]  WITH CHECK ADD  CONSTRAINT [FK_Assign_Profiles] FOREIGN KEY([ProfileID])
REFERENCES [dbo].[Profiles] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign] CHECK CONSTRAINT [FK_Assign_Profiles]
GO
/****** Object:  ForeignKey [FK_Assign_Users]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign]  WITH CHECK ADD  CONSTRAINT [FK_Assign_Users] FOREIGN KEY([TeacherID])
REFERENCES [dbo].[Users] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assign_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assign]'))
ALTER TABLE [dbo].[Assign] CHECK CONSTRAINT [FK_Assign_Users]
GO
/****** Object:  ForeignKey [FK_Answers_Questions]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Answers_Questions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Answers]'))
ALTER TABLE [dbo].[Answers]  WITH CHECK ADD  CONSTRAINT [FK_Answers_Questions] FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Questions] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Answers_Questions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Answers]'))
ALTER TABLE [dbo].[Answers] CHECK CONSTRAINT [FK_Answers_Questions]
GO
/****** Object:  ForeignKey [FK_TestQuestions_Questions]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestQuestions_Questions]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestQuestions]'))
ALTER TABLE [dbo].[TestQuestions]  WITH CHECK ADD  CONSTRAINT [FK_TestQuestions_Questions] FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Questions] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestQuestions_Questions]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestQuestions]'))
ALTER TABLE [dbo].[TestQuestions] CHECK CONSTRAINT [FK_TestQuestions_Questions]
GO
/****** Object:  ForeignKey [FK_TestQuestions_Tests]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestQuestions_Tests]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestQuestions]'))
ALTER TABLE [dbo].[TestQuestions]  WITH CHECK ADD  CONSTRAINT [FK_TestQuestions_Tests] FOREIGN KEY([TestID])
REFERENCES [dbo].[Tests] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestQuestions_Tests]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestQuestions]'))
ALTER TABLE [dbo].[TestQuestions] CHECK CONSTRAINT [FK_TestQuestions_Tests]
GO
/****** Object:  ForeignKey [FK_TestAnswers_Answers]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestAnswers_Answers]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestAnswers]'))
ALTER TABLE [dbo].[TestAnswers]  WITH CHECK ADD  CONSTRAINT [FK_TestAnswers_Answers] FOREIGN KEY([AnswerID])
REFERENCES [dbo].[Answers] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestAnswers_Answers]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestAnswers]'))
ALTER TABLE [dbo].[TestAnswers] CHECK CONSTRAINT [FK_TestAnswers_Answers]
GO
/****** Object:  ForeignKey [FK_TestAnswers_TestQuestions]    Script Date: 04/23/2013 16:16:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestAnswers_TestQuestions]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestAnswers]'))
ALTER TABLE [dbo].[TestAnswers]  WITH CHECK ADD  CONSTRAINT [FK_TestAnswers_TestQuestions] FOREIGN KEY([TestQuestionID])
REFERENCES [dbo].[TestQuestions] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TestAnswers_TestQuestions]') AND parent_object_id = OBJECT_ID(N'[dbo].[TestAnswers]'))
ALTER TABLE [dbo].[TestAnswers] CHECK CONSTRAINT [FK_TestAnswers_TestQuestions]
GO
