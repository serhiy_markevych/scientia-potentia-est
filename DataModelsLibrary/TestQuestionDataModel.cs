﻿using System;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Test_questions"
    /// </summary>
    [Serializable]
    public sealed class TestQuestionDataModel : IEquatable<TestQuestionDataModel>
    {
        public int ID { get; set; }
        public int TestID { get; set; }
        public int QuestionID { get; set; }
        public int OrdinalNumber { get; set; }

        #region Constructors

        public TestQuestionDataModel()
        { }

        public TestQuestionDataModel(int testID, int questionID, int ordinalNumber = 1)
        {
            ID = 0;
            TestID = testID;
            QuestionID = questionID;
            OrdinalNumber = ordinalNumber;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(TestQuestionDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestQuestionDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(TestQuestionDataModel operandA, TestQuestionDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(TestQuestionDataModel operandA, TestQuestionDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
