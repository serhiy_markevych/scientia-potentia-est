﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModelsLibrary
{
    /// <summary>
    /// Data Model for tests profiles
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>13.05.2013</date>
    [Serializable]
    public class RatingScaleDataModel : IEquatable<RatingScaleDataModel>
    {
        #region Fields
        private int _id;
        private ProfilesDataModel _profile;
        private string _text;
        private decimal _lowbound;
        #endregion

        #region Properties
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public ProfilesDataModel Profile
        {
            get { return _profile; }
            set { _profile = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public decimal LowBound
        {
            get { return _lowbound; }
            set { _lowbound = value; }
        }
        #endregion

        #region Constructors
        public RatingScaleDataModel()
        {
            _id = 0;
            _profile = null;
            _text = "";
            _lowbound = 0;
        }

        public RatingScaleDataModel(ProfilesDataModel profile, string text, decimal lowbound, int id = 0)
        {
            _id = id;
            _profile = profile;
            _text = text;
            _lowbound = lowbound;
        }
        #endregion

        #region IEquatable implementation
        public bool Equals(RatingScaleDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.Text.Equals(other.Text) && this.LowBound.Equals(other.LowBound);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as RatingScaleDataModel);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public static bool operator ==(RatingScaleDataModel operandA, RatingScaleDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(RatingScaleDataModel operandA, RatingScaleDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion

        #region Copying methods
        public void Copy(RatingScaleDataModel ratingscale)
        {
            this._id = ratingscale.ID;
            this._profile = ratingscale.Profile;
            this._text = ratingscale.Text;
            this._lowbound = ratingscale.LowBound;
        }

        public static RatingScaleDataModel CreateFrom(RatingScaleDataModel ratingscale)
        {
            return new RatingScaleDataModel() { _id = ratingscale.ID, _profile = ratingscale.Profile, _text = ratingscale.Text, _lowbound = ratingscale.LowBound };
        }
        #endregion
    }
}
