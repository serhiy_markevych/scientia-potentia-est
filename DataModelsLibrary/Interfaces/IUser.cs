﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModelsLibrary.Interfaces
{
    /// <summary>
    /// User Interface for base properties and methods
    /// </summary>
    /// <autor>Cherednychok Ivan</autor>
    /// <date>15 April 2013</date>
    public interface IUser
    {
        int ID { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string FullName { get; }
        List<RoleDataModel> Roles { get; set; }
        bool HasRole(UserRole role);
    }
}
