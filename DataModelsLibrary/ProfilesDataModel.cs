﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModelsLibrary
{
    /// <summary>
    /// Data Model for tests profiles
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>13.05.2013</date>
    [Serializable]
    public class ProfilesDataModel : IEquatable<ProfilesDataModel>
    {
        #region Fields
        private int _id;
        private string _name;
        private int _canceledmark;
        private int _timelimit;
        private int _shufflequestions;
        private int _shuffleanswers;
        private int _questionscount;
        private ProfileMarkType _marktype;
        #endregion

        #region Properties
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int CanceledMark
        {
            get { return _canceledmark; }
            set { _canceledmark = value; }
        }

        public int TimeLimit
        {
            get { return _timelimit; }
            set { _timelimit = value; }
        }

        public int ShuffleQuestions
        {
            get { return _shufflequestions; }
            set { _shufflequestions = value; }
        }

        public int ShuffleAnswers
        {
            get { return _shuffleanswers; }
            set { _shuffleanswers = value; }
        }

        public int QuestionsCount
        {
            get { return _questionscount; }
            set { _questionscount = value; }
        }

        public ProfileMarkType MarkType
        {
            get { return _marktype; }
            set { _marktype = value; }
        }
        #endregion

        #region Constructors
        public ProfilesDataModel()
        {
            _id = 0;
            _name = "";
            _canceledmark = 0;
            _timelimit = 0;
            _shufflequestions = 0;
            _shuffleanswers = 0;
            _questionscount = 0;
            _marktype = ProfileMarkType.Percent;
        }

        public ProfilesDataModel(string name, int canceledmark, int timelimit, int shufflequestions, int shuffleanswers, int questionscount, ProfileMarkType marktype, int id = 0)
        {
            _id = id;
            _name = name;
            _canceledmark = canceledmark;
            _timelimit = timelimit;
            _shufflequestions = shufflequestions;
            _shuffleanswers = shuffleanswers;
            _questionscount = questionscount;
            _marktype = marktype;
        }
        #endregion

        #region IEquatable implementation
        public bool Equals(ProfilesDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.Name.Equals(other.Name) && this.CanceledMark.Equals(other.CanceledMark) && this.TimeLimit.Equals(other.TimeLimit) && this.ShuffleQuestions.Equals(other.ShuffleQuestions) && this.ShuffleAnswers.Equals(other.ShuffleAnswers) && this.QuestionsCount.Equals(other.QuestionsCount) && this.MarkType.Equals(other.MarkType);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ProfilesDataModel);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public static bool operator ==(ProfilesDataModel operandA, ProfilesDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(ProfilesDataModel operandA, ProfilesDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion

        #region Copying methods
        public void Copy(ProfilesDataModel profile)
        {
            this._id = profile.ID;
            this._name = profile.Name;
            this._canceledmark = profile.CanceledMark;
            this._timelimit = profile.TimeLimit;
            this._shufflequestions = profile.ShuffleQuestions;
            this._shuffleanswers = profile.ShuffleAnswers;
            this._questionscount = profile.QuestionsCount;
            this._marktype = profile.MarkType;
        }

        public static ProfilesDataModel CreateFrom(ProfilesDataModel profile)
        {
            return new ProfilesDataModel() { _id = profile.ID, _name = profile.Name, _canceledmark = profile.CanceledMark, _timelimit = profile.TimeLimit, _shufflequestions = profile.ShuffleQuestions, _shuffleanswers = profile.ShuffleAnswers, _questionscount = profile.QuestionsCount, _marktype = profile.MarkType };
        }
        #endregion
    }
}
