﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using ResourcesLibrary;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Subjects".
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>15.04.2013</date>
    [XmlType("Subject")]
    public class SubjectDataModel : IEquatable<SubjectDataModel>
    {
        #region Fields

        private int _id;
        private string _name;
        private int _canceledMark;

        #endregion

        #region Properties

        [XmlAttribute("ID")]
        public int ID 
        {
            get { return this._id; }
            set { this._id = value; }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                if (!Validator.IsValid(value))
                    throw new ArgumentException(Errors.LenghtName);
                this._name = value;
            }
        }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark
        {
            get { return this._canceledMark; }
            set { this._canceledMark = Validator.DbCanceledMarkToOneZero(value); }
        }

        #endregion

        #region Constructors

        public SubjectDataModel() 
        {
            this._id = 0;
            this._canceledMark = 0;
            this._name = "";
        }

        public SubjectDataModel(int id, string name, int canceledMark = 0)
        {
            ID = id;
            Name = name;
            CanceledMark = canceledMark;
        }

        public SubjectDataModel(string name, int canceled_mark = 0)
            :this(0, name, canceled_mark)
        { }

        #endregion

        #region IEquatable implementation

        public bool Equals(SubjectDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.Name.Equals(other.Name) && this.CanceledMark.Equals(other.CanceledMark);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as SubjectDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(SubjectDataModel operandA, SubjectDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(SubjectDataModel operandA, SubjectDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion

        #region Copying methods

        /// <summary>
        /// Copy data from input data model to this (without properties validation).
        /// </summary>
        /// <param name="subjectDM">Data model to copy</param>
        public void CopyFrom(SubjectDataModel subjectDM)
        {
            this._id = subjectDM.ID;
            this._name = subjectDM.Name;
            this._canceledMark = subjectDM.CanceledMark;
        }

        /// <summary>
        /// Create a clone of the data model (without properties validation).
        /// </summary>
        /// <param name="subjectDM">Data model to cloning</param>
        /// <returns>Clone of the input data model</returns>
        public static SubjectDataModel CreateFrom(SubjectDataModel subjectDM)
        {
            //return new SubjectDataModel(subjectDM.ID, subjectDM.Name, subjectDM.CanceledMark, false);
            return new SubjectDataModel() {_id = subjectDM.ID, _name = subjectDM.Name, _canceledMark = subjectDM._canceledMark };
        } 

        #endregion

        public bool IsValid()
        {
            return Validator.IsValid(this.Name);
        }
    }
}
