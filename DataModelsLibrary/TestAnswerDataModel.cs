﻿using System;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Test_answers"
    /// </summary>
    [Serializable]
    public sealed class TestAnswerDataModel : IEquatable<TestAnswerDataModel>
    {
        public int ID { get; set; }
        public int TestQuestionID { get; set; }
        public int? AnswerID { get; set; }
        public int OrdinalNumber { get; set; }
        public string Text { get; set; }

        #region Constructors

        public TestAnswerDataModel()
        { }

        public TestAnswerDataModel(int testQuestionID, int? answerID, int ordinalNumber = 1, string text = "")
        {
            ID = 0;
            TestQuestionID = testQuestionID;
            AnswerID = answerID;
            OrdinalNumber = ordinalNumber;
            Text = text;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(TestAnswerDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestAnswerDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(TestAnswerDataModel operandA, TestAnswerDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(TestAnswerDataModel operandA, TestAnswerDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
