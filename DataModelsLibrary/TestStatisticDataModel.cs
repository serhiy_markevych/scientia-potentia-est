﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResourcesLibrary;

namespace DataModelsLibrary
{
    /// <summary>
    /// Data model for statistic of the test
    /// </summary>
    /// <autor>Cherednychok Ivan</autor>
    /// <date>11 May 2013</date>
    [Serializable]
    public class TestStatisticDataModel
    {
        #region Fields
        private string _title;
        private string _author; // who is creator of the test
        private string _subject; // examination subject
        private DateTime _dateOfPassing; // examination date
        private string _userFullName; // user who passed the exam
        private string _userGroup;
        private int _totalQuestions; // count of all questions in test
        private int _totalCorrectAnswers; // count of success passed question (must be <= _totalQuestions)
        private bool _isPassed; // test failed or success
        private List<QuestionInfo> _questionsInfo; // info about each question in test
        #endregion

        #region Constructors
        public TestStatisticDataModel() 
        {

        }
        #endregion

        #region Methods
        public override string ToString()
        {
            string testResult;
            if (IsPassed) testResult = "passed";
            else testResult = "failed";

            return String.Format(Messages.TestStatistic,
                _title, _subject, _author, _userFullName, _userGroup, _totalCorrectAnswers, _totalQuestions, testResult);
        }
        #endregion

        #region Properties
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public DateTime DateOfPassing
        {
            get { return _dateOfPassing; }
            set { _dateOfPassing = value; }
        }
        public string UserFullName
        {
            get { return _userFullName; }
            set { _userFullName = value; }
        }
        public string UserGroup
        {
            get { return _userGroup; }
            set { _userGroup = value; }
        }
        public int TotalQuestions
        {
            get { return _totalQuestions; }
            set { _totalQuestions = value; }
        }
        public int TotalCorrectAnswers
        {
            get { return _totalCorrectAnswers; }
            set { _totalCorrectAnswers = value; }
        }
        public bool IsPassed
        {
            get { return _isPassed; }
            set { _isPassed = value; }
        }
        public List<QuestionInfo> QuestionsInfo
        {
            get { return _questionsInfo; }
            set { _questionsInfo = value; }
        }
        #endregion
    }

    /// <summary>
    /// Information about the test question
    /// </summary>
    /// <autor>Cherednychok Ivan</autor>
    /// <date>11 May 2013</date>
    [Serializable]
    public class QuestionInfo
    {
        public int OrdinalNumber { get; set; }
        public string Title { get; set; }
        public bool IsCorrect { get; set; }
        public string QuestionResult
        {
            get
            {
                if (IsCorrect)
                {

                    return DisplayLabels.QuestionPassedYes;
                }
                else
                {
                    return DisplayLabels.QuestionPassedNo;
                }
            }
        }
    }
}
