﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Answers"
    /// </summary>
    /// <author>Chernysh Denis</author>
    /// <date>15 May 2013</date>
    public class AnswerDataModel : IEquatable<AnswerDataModel>
    {

        #region Fields

        private int _id;
        private int _questionId;
        private int _ordinalNumber;
        private string _text;
        private int _significance;
        private string _group;
        private int _canceledMark;

        private bool _isChecked;

        #endregion

        #region Properties

        public int ID
        {
            get { return this._id; }
            set { this._id = value; }
        }

        public int QuestionID
        {
            get { return this._questionId; }
            set { this._questionId = value; }
        }

        public int OrdinalNumber
        {
            get { return this._ordinalNumber; }
            set { this._ordinalNumber = value; }
        }

        public string Text
        {
            get { return this._text; }
            set { this._text = value; }
        }

        public int Significance
        {
            get { return this._significance; }
            set { this._significance = value; }
        }

        public string Group
        {
            get { return this._group; }
            set { this._group = value; }
        }

        public int CanceledMark
        {
            get { return this._canceledMark; }
            set { this._canceledMark = Validator.DbCanceledMarkToOneZero(value); }
        }
        /// <summary>
        /// use in class OneAnswerUI and ManyAnswerUI for story curent state
        /// </summary>
        public bool IsChecked
        {
            get { return this._isChecked; }
            set { this._isChecked = value; }
        }

        #endregion

        #region Constructors

        public AnswerDataModel()
        {
            this._id = 0;
            this._questionId = 0;
            this._ordinalNumber = 0;
            this._text = "";
            this._significance = 0;
            this._group = "";
            this._canceledMark = 0;
            this._isChecked = false;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(AnswerDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.QuestionID.Equals(other.QuestionID)
                && this.OrdinalNumber.Equals(other.OrdinalNumber) && this.Text.Equals(other.Text)
                && this.Significance.Equals(other.Significance) && this.Group.Equals(other.Group)
                && this.CanceledMark.Equals(other.CanceledMark);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as AnswerDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(AnswerDataModel operandA, AnswerDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(AnswerDataModel operandA, AnswerDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion

    }
}
