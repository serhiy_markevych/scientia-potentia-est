﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using ResourcesLibrary;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Moduls"
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>15.04.2013</date>
    [XmlType("Modul")]
    public class ModulDataModel : IEquatable<ModulDataModel>
    {
        #region Fields

        private int _id;
        private string _name;
        private int _canceledMark;
        private SubjectDataModel _subject;

        #endregion

        #region Properties

        [XmlAttribute("ID")]
        public int ID
        { 
            get { return this._id; }
            set { this._id = value; }
        }
        [XmlElement("Name")]
        public string Name 
        {
            get { return _name; }
            set 
            {
                if (!Validator.IsValid(value))
                    throw new ArgumentException(Errors.LenghtName);
                this._name = value;
            }
        }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark 
        {
            get { return this._canceledMark; }
            set { this._canceledMark = Validator.DbCanceledMarkToOneZero(value); }
        }

        public SubjectDataModel Subject 
        {
            get { return this._subject; }
            set { this._subject = value; }
        }

        #endregion

        #region Constructors

        public ModulDataModel()
        {
            this._id = 0;
            this._canceledMark = 0;
            this._name = "";
            this._subject = null;
        }

        public ModulDataModel(int id, string name, SubjectDataModel subject, int canceledMark = 0)
        {
            this.ID = id;
            this.Subject = subject;
            this.Name = name;
            this.CanceledMark = canceledMark;
        }

        public ModulDataModel(string name, SubjectDataModel subject, int canceled_mark = 0)
            : this(0, name, subject, canceled_mark)
        { }

        #endregion

        #region IEquatable implementation

        public bool Equals(ModulDataModel other)
        {
            if (other == null)
                return false;
            SubjectDataModel thisSubject = (this.Subject != null) ? this.Subject : new SubjectDataModel();
            SubjectDataModel otherSubject = (other.Subject != null) ? other.Subject : new SubjectDataModel();
            return this.ID.Equals(other.ID) && this.Name.Equals(other.Name)
                && this.CanceledMark.Equals(other.CanceledMark) && thisSubject.Equals(otherSubject);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ModulDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(ModulDataModel operandA, ModulDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(ModulDataModel operandA, ModulDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion

        #region Copying methods

        /// <summary>
        /// Copy data from input data model to this.
        /// </summary>
        /// <param name="modulDM">Data model to copy</param>
        public void CopyFrom(ModulDataModel modulDM)
        {
            this._id = modulDM.ID;
            this._name = modulDM.Name;
            this._canceledMark = modulDM.CanceledMark;
            this._subject = modulDM.Subject;
        }

        /// <summary>
        /// Create a clone of the data model
        /// </summary>
        /// <param name="subjectDM">Data model to cloning</param>
        /// <returns>Clone of the input data model</returns>
        public static ModulDataModel CreateFrom(ModulDataModel modulDM)
        {
            return new ModulDataModel() {_id = modulDM.ID, _name = modulDM.Name, _canceledMark = modulDM.CanceledMark, _subject = modulDM.Subject};
        }

        #endregion

        public bool IsValid()
        {
            return Validator.IsValid(this.Name) && (this.Subject != null);
        }
    }
}
