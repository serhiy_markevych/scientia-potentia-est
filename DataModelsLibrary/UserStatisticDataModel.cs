﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResourcesLibrary;

namespace DataModelsLibrary
{
    /// <summary>
    /// Data model for user statistic
    /// </summary>
    /// <author>Cherednychok Ivan</author>
    /// <date>12 May 2013</date>
    public class UserStatisticDataModel
    {
        private string _subject;
        private List<TestInfo> _testInfo;

        public UserStatisticDataModel() {
            _subject = "";
            _testInfo = new List<TestInfo>();
        }

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public List<TestInfo> TestInfo
        {
            get { return _testInfo; }
            set { _testInfo = value; }
        }
        
    }


    /// <summary>
    /// Information about the test
    /// </summary>
    /// <autor>Cherednychok Ivan</autor>
    /// <date>12 May 2013</date>
    public class TestInfo 
    {
        public string TestTitle { get; set; }
        public string AuthorName { get; set; }
        public bool IsPassed { get; set; }
        public string TestResult
        {
            get
            {
                if (IsPassed)
                {
                    return DisplayLabels.TestPassedYes;
                }
                else
                {
                    return DisplayLabels.TestPassedNo;
                }
            }
        }
        public DateTime DateOfPassing { get; set; }
        public string StudentName { get; set; }
        public string GroupName { get; set; }
    }
}
