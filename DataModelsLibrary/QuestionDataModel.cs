﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResourcesLibrary;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Questions"
    /// </summary>
    /// <author>Chernysh Denis</author>
    /// <date>15 May 2013</date>

    public class QuestionDataModel: IEquatable<QuestionDataModel>
    {

        #region Fields

        private int _id;
        private int _ordinalNumber;
        private string _text;
        private int _significance;
        private QuestionType _type;
        private int _modul_id;
        private int _canceledMark;
        private string _explanation;

        #endregion

        #region Properties

        public int ID
        {
            get { return this._id; }
            set { this._id = value; }
        }

        public int OrdinalNumber
        {
            get { return this._ordinalNumber; }
            set { this._ordinalNumber = value; }
        }

        public string Text
        {
            get { return this._text; }
            set { this._text = value; }
        }

        public int Significance
        {
            get { return this._significance; }
            set { this._significance = value; }
        }

        public QuestionType Type
        {
            get { return this._type; }
            set { this._type = value; }
        }

        public int ModulID
        {
            get { return this._modul_id; }
            set { this._modul_id = value; }
        }

        public int CanceledMark
        {
            get { return this._canceledMark; }
            set { this._canceledMark = Validator.DbCanceledMarkToOneZero(value); }
        }

        public string Explanation
        {
            get { return this._explanation; }
            set
            {
                if (!Validator.IsValid(value))
                    throw new ArgumentException(Errors.LenghtName);
                this._explanation = value;
            }
        }

        #endregion

        #region Constructors

        public QuestionDataModel()
        {
            this._id = 0;
            this._ordinalNumber = 0;
            this._text = "";
            this._significance = 0;
            this._type = QuestionType.OneAnswer;
            this._modul_id = 0;
            this._canceledMark = 0;
            this._explanation = "";
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(QuestionDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.OrdinalNumber.Equals(other.OrdinalNumber)
                && this.Text.Equals(other.Text) && this.Significance.Equals(other.Significance) 
                && this.Type.Equals(other.Type) && this.ModulID.Equals(other.ModulID) 
                && this.CanceledMark.Equals(other.CanceledMark) && this.Explanation.Equals(other.Explanation);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as QuestionDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(QuestionDataModel operandA, QuestionDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(QuestionDataModel operandA, QuestionDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        #endregion
    }
}
