﻿using System;

namespace DataModelsLibrary
{
    /// <summary>
    /// Record in a table "Tests"
    /// </summary>
    [Serializable]
    public sealed class TestDataModel : IEquatable<TestDataModel>
    {
        private TimeSpan _duration;

        public int ID { get; set; }
        public int ModulID { get; set; }
        public int StudentID { get; set; }
        public int TeacherID { get; set; }
        public int ProfileID { get; set; }
        public TestMode Mode { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        public long DurationTicks
        {
            get { return _duration.Ticks; }
            set { _duration = new TimeSpan(value); }
        }
        public decimal Rating { get; set; }
        public decimal TotalRating { get; set; }
        public int TotalQuestions { get; set; }
        public int TotalAnswers { get; set; }

        #region Constructors

        public TestDataModel()
        { }

        public TestDataModel(int modulID, int studentID, int teacherID, int profileID, TestMode mode, DateTime date, TimeSpan duration, int rating = 0, 
            int totalRating = 0, int totalQuestions = 0, int totalAnswers = 0)
        {
            ID = 0;
            ModulID = modulID;
            StudentID = studentID;
            TeacherID = teacherID;
            ProfileID = profileID;
            Mode = mode;
            Date = date;
            Duration = duration;
            Rating = rating;
            TotalRating = totalRating;
            TotalQuestions = totalQuestions;
            TotalAnswers = totalAnswers;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(TestDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(TestDataModel operandA, TestDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(TestDataModel operandA, TestDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
