﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModelsLibrary
{
    /// <summary>
    /// Validation methods for data models.
    /// </summary>
    /// <author>M. Saharilenko</author>
    /// <date>15.04.2013</date>
    public static class Validator
    {
        /// <summary>
        /// For string fields validation.
        /// </summary>
        /// <param name="value">String value</param>
        /// <param name="minLength">Min required string lenght</param>
        /// <param name="maxLength">Max possible string lenght</param>
        /// <returns>Validation success</returns>
        public static bool IsValid(string value, int minLength = 2, int maxLength = 100)
        {
            bool res = false;
            if(!string.IsNullOrEmpty(value))
                res = value.Length >= minLength && value.Length <= maxLength;
            return res;
        }

        /// <summary>
        /// Convert value of canceled_mark field from database to bool.
        /// </summary>
        /// <param name="dbCanceledMark">Value of canceled_mark in database</param>
        /// <returns>Bool equivalent of dbCanceledMark</returns>
        public static bool DbCanceledMarkToBool(int dbCanceledMark)
        {
            return dbCanceledMark > 0;
        }

        /// <summary>
        /// Convert value of canceled_mark field from database to one/zero format.
        /// </summary>
        /// <param name="dbCanceledMark">Value of canceled_mark in database</param>
        /// <returns>One/zero formatted equivalent of dbCanceledMark</returns>
        public static int DbCanceledMarkToOneZero(int dbCanceledMark)
        {
            int result = 0;
            if (dbCanceledMark > 0)
                result = 1;
            return result;
        }
    }
}
