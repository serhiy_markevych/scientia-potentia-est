﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModelsLibrary
{
    /// <summary>
    /// Data Model for tests profiles
    /// </summary>
    /// <author>Sergei Labzov</author>
    /// <date>13.05.2013</date>
    [Serializable]
    public class AssignDataModel : IEquatable<AssignDataModel>
    {
        #region Fields
        private int _id;
        private GroupDataModel _group;
        private UserDataModel _teacher;
        private ProfilesDataModel _profile;
        private ModulDataModel _module;
        private DateTime? _testdate;
        #endregion

        #region Properties
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public GroupDataModel Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public UserDataModel Teacher
        {
            get { return _teacher; }
            set { _teacher = value; }
        }

        public ProfilesDataModel Profile
        {
            get { return _profile; }
            set { _profile = value; }
        }

        public ModulDataModel Module
        {
            get { return _module; }
            set { _module = value; }
        }

        public DateTime? TestDate
        {
            get { return _testdate; }
            set { _testdate = value; }
        }
        #endregion

        #region Constructors
        public AssignDataModel()
        {
            _id = 0;
            _group = null;
            _teacher = null;
            _profile = null;
            _module = null;
            _testdate = null;
        }

        public AssignDataModel(GroupDataModel group, UserDataModel teacher, ProfilesDataModel profile, ModulDataModel module, DateTime testdate, int id = 0)
        {
            _id = id;
            _group = group;
            _teacher = teacher;
            _profile = profile;
            _module = module;
            _testdate = testdate;
        }
        #endregion

        #region IEquatable implementation
        public bool Equals(AssignDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.Group.Equals(other.Group) && this.Teacher.Equals(other.Teacher) && this.Profile.Equals(other.Profile) && this.Module.Equals(other.Module) && this.TestDate.Equals(other.TestDate);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as AssignDataModel);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public static bool operator ==(AssignDataModel operandA, AssignDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(AssignDataModel operandA, AssignDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion

        #region Copying methods
        public void Copy(AssignDataModel assign)
        {
            this._id = assign.ID;
            this._group = assign.Group;
            this._teacher = assign.Teacher;
            this._profile = assign.Profile;
            this._module = assign.Module;
            this._testdate = assign.TestDate;
        }

        public static AssignDataModel CreateFrom(AssignDataModel assign)
        {
            return new AssignDataModel() { _id = assign.ID, _group = assign.Group, _teacher = assign.Teacher, _profile = assign.Profile, _module = assign.Module, _testdate = assign.TestDate };
        }
        #endregion
    }
}
